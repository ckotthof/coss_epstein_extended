# Created 2020 by Group "Left as an Exercise to the Reader"
# for 851-0101-86L "Complex Social Systems: Modeling Agents, Learning, and Games"
# Semester: Winter 2020/2021
# ETH Zurich
#
# with help of Mesa framework
# based on Epstein's Civil Violence model

#running this file with python lets you run the model (with specified parameters) without the visualization. The Actives and Prisoners of each timestep are saved into a file with a specific format in the output folder. you can either take the mean from several runs or save all individual runs in files.
from our_code.model import EpsteinModel
import sys

if (len(sys.argv)==1):
	print("\nUsage: '$ python "+sys.argv[0]+" mean=<True/False> sampleruns=<value> [arglist..]'\nPossible Arguments:\n    height=<value>                    40\n    width=<value>                     40\n    max_iters=<value>                 1000\n    citizen_density=<value>           0.7\n    max_jail_sentence=<value>         30\n    arrest_prob_constant=<value>      2.3\n    active_threshold=<value>          0.1\n    moore=<bool>                      False\n    legitimacy=<value>                0.7\n    move_towards_same_hardship=<bool> False\n    police_movement_type=<value>      0\n    citizen_movement_type=<value>     0\n    random_threshold=<value>          0.05\n    police_count=<value>              40\n    strong_formation=<bool>           False\n")
else:
	mean = sys.argv[1].split("=")
	if (mean[0] != "mean" or (mean[1]!="True" and mean[1]!="False")):
		print("wrong format of 'mean'")
		exit()

	sampleruns = sys.argv[2].split("=")
	if (sampleruns[0] != "sampleruns" or int(sampleruns[1])==0):
		print("wrong format of 'sampleruns'")
		exit()

	optional_params={}

	for i in range(3, len(sys.argv)):
		splitted = sys.argv[i].split("=")
		parameter = splitted[0]
		value = splitted[1]

		if parameter=="height":
			optional_params["height"]=int(value)
		elif parameter=="width":
			optional_params["width"]=int(value)
		elif parameter=="max_iters":
			optional_params["max_iters"]=int(value)
		elif parameter=="citizen_density":
			optional_params["citizen_density"]=float(value)
		elif parameter=="max_jail_sentence":
			optional_params["max_jail_sentence"]=int(value)
		elif parameter=="arrest_prob_constant":
			optional_params["arrest_prob_constant"]=float(value)
		elif parameter=="active_threshold":
			optional_params["active_threshold"]=float(value)
		elif parameter=="moore":
			optional_params["moore"]=True if (value=="True") else False
		elif parameter=="legitimacy":
			optional_params["legitimacy"]=float(value)
		elif parameter=="move_towards_same_hardship":
			optional_params["move_towards_same_hardship"]=True if (value=="True") else False
		elif parameter=="police_movement_type":
			optional_params["police_movement_type"]=int(value)
		elif parameter=="citizen_movement_type":
			optional_params["citizen_movement_type"]=int(value)
		elif parameter=="random_threshold":
			optional_params["random_threshold"]=float(value)
		elif parameter=="police_count":
			optional_params["police_count"]=int(value)
		elif parameter=="strong_formation":
			optional_params["strong_formation"]=True if (value=="True") else False
		else:
			print("Parameter: '"+parameter+"' not found")
			exit()

	iters = 1000
	if "max_iters" in optional_params:
		iters = optional_params["max_iters"]
	police_movement_type = 0
	if "police_movement_type" in optional_params:
		police_movement_type = optional_params["police_movement_type"]
	legitimacy = 0.7
	if "legitimacy" in optional_params:
		legitimacy = optional_params["legitimacy"]
	strong_formation = 0
	if "strong_formation" in optional_params:
		if optional_params["strong_formation"]==True:
			strong_formation = 1
	max_jail_sentence = 30
	if "max_jail_sentence" in optional_params:
		max_jail_sentence = optional_params["max_jail_sentence"]
	citizen_movement_type = 0
	if "citizen_movement_type" in optional_params:
		citizen_movement_type = optional_params["citizen_movement_type"]

	import pandas as pd
	import numpy as np
	import os

	if not os.path.exists('output'):
		os.makedirs('output')


	if mean[1]=="True":
		dfs = []
		print("---------------------------------------------------------------------------------------------------\n"+
			  "starting: extracted_data/output/pm"+str(police_movement_type)+"_f"+str(strong_formation)+"_l"+("%.4f" % legitimacy)+"_cm"+str(citizen_movement_type)+"_js"+str(max_jail_sentence)+".txt with "+str(iters)+" iterations per model, taking mean of "+sampleruns[1]+" runs...")

		for i in range(int(sampleruns[1])):
			model = EpsteinModel(**optional_params)

			for t in range(iters):
				model.step()

			dfs.append(model.datacollector.get_model_vars_dataframe())
			print("finished "+str(i+1)+". model run")


		pd.concat(dfs).groupby(level=0).mean().to_csv(r"extracted_data/output/pm"+str(police_movement_type)+"_f"+str(strong_formation)+"_l"+("%.4f" % legitimacy)+"_cm"+str(citizen_movement_type)+"_js"+str(max_jail_sentence)+".txt",  index=False, sep=' ', mode='w')
		print("finished extracted_data/output/pm"+str(police_movement_type)+"_f"+str(strong_formation)+"_l"+("%.4f" % legitimacy)+"_cm"+str(citizen_movement_type)+"_js"+str(max_jail_sentence) +".txt\n---------------------------------------------------------------------------------------------------")
	else:
		print("---------------------------------------------------------------------------------------------------")

		for i in range(int(sampleruns[1])):
			print("starting: extracted_data/output/pm"+str(police_movement_type)+"_f"+str(strong_formation)+"_l"+("%.4f" % legitimacy)+"_cm"+str(citizen_movement_type)+"_js"+str(max_jail_sentence)+str(i)+".txt")

			model = EpsteinModel(**optional_params)

			for t in range(iters):
				model.step()
			model.datacollector.get_model_vars_dataframe().to_csv(r"extracted_data/output/pm"+str(police_movement_type)+"_f"+str(strong_formation)+"_l"+("%.4f" % legitimacy)+"_cm"+str(citizen_movement_type)+"_js"+str(max_jail_sentence)+str(i)+".txt",  index=False, sep=' ', mode='w')

		print("finished\n---------------------------------------------------------------------------------------------------")
