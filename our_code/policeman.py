# Created 2020 by Group "Left as an Exercise to the Reader"
# for 851-0101-86L "Complex Social Systems: Modeling Agents, Learning, and Games"
# Semester: Winter 2020/2021
# ETH Zurich
#
# with help of Mesa framework
# based on Epstein's Civil Violence model

import math
import sys
from mesa import Agent
from our_code.moving_agent import MovingAgent


#Class of the police agents, that try to lock up active citizens.
class PoliceMan(MovingAgent):

    def __init__( self, unique_id, model):
        super().__init__(unique_id, model)
        self.type = "Police"
        self.model = model
        self.min_rioter = None
        self.max_rioter = None
        self.max_grievance = 0
        self.arrestee_pos = (-1,-1)

    def step(self): #this method is called each step for each police agent. with the in the model parameters specified police_movement_type, the police man then decides which movement pattern it has to follow
        if self.model.police_movement_type == 0: #random
            self.police_random_movement()
        elif self.model.police_movement_type == 1: #random towards rioters
            self.police_towards_rioter_movement()
        elif self.model.police_movement_type == 2: #1 line bottom up
            self.police_sweep_movement()
        elif self.model.police_movement_type == 3: #2 lines towards each other
            self.police_sweep_movement_2()
        elif self.model.police_movement_type == 4: #take arrestees pos (or random if no actives)
            self.police_take_arrestees_pos()
        elif self.model.police_movement_type == 5: #2x diagonal
            self.police_diagonal()



    #mostly straight forward implementation. for specifics, consult our paper, where these movement types are described.

    def police_random_movement(self):
        self.update_neighbourhood()
        self.arrest_random()
        self.random_move()

    def police_towards_rioter_movement(self):
        self.update_police_neighbourhood()
        self.arrest_random()
        if not self.max_rioter is None:
            self.max_rioter.update_neighbourhood()
            if self.max_rioter.empty_neighbours and self.random.random() > self.model.random_threshold:
                new_pos = self.random.choice(self.max_rioter.empty_neighbours)
                self.model.grid.move_agent(self, new_pos)
                return
            if self.min_rioter is None or self.random.random() < self.model.random_threshold:
                self.random_move()
            else:
                self.swap_move(self.min_rioter)

    def police_sweep_movement(self):
        self.update_neighbourhood()
        self.arrest_random()
        upper_cell_x = self.pos[0]
        upper_cell_y = (self.pos[1]+1) % self.model.height
        upper_cell_pos = ( upper_cell_x, upper_cell_y )
        if self.model.grid.is_cell_empty(upper_cell_pos):
            self.model.grid.move_agent(self,upper_cell_pos)
        else:
            upper_agent = self.model.grid[upper_cell_x][upper_cell_y]
            self.swap_move(upper_agent)

    def police_sweep_movement_2(self):
        self.update_neighbourhood()
        self.arrest_random()

        cell_x = self.pos[0]
        if (cell_x%2 == 0):
            cell_y = (self.pos[1]+1) % self.model.height
        else:
            cell_y = (self.pos[1]-1) % self.model.height

        cell_pos = ( cell_x, cell_y )
        if self.model.grid.is_cell_empty(cell_pos):
            self.model.grid.move_agent(self,cell_pos)
        else:
            upper_agent = self.model.grid[cell_x][cell_y]
            self.swap_move(upper_agent)

    def police_take_arrestees_pos(self):
        self.update_neighbourhood()
        self.arrest_random()
        if self.arrestee_pos != (-1,-1):
            #has arrested someone
            self.model.grid.move_agent(self, self.arrestee_pos)
        else:
            #has not arrested anyone
            self.random_move()

    def police_diagonal(self):
        self.update_neighbourhood()
        self.arrest_random()

        cell_x = self.pos[0]
        cell_y = self.pos[1]
        if ((cell_x+cell_y)%2 == 0): #diagonal from bottom left to upper right
            cell_x = (cell_x-1) % self.model.width
            cell_y = (cell_y+1) % self.model.height
        else: #the other diagonal
            cell_x = (cell_x+1) % self.model.width
            cell_y = (cell_y+1) % self.model.height

        cell_pos = ( cell_x, cell_y )
        if self.model.grid.is_cell_empty(cell_pos):
            self.model.grid.move_agent(self,cell_pos)
        else:
            upper_agent = self.model.grid[cell_x][cell_y]
            self.swap_move(upper_agent)


    def update_police_neighbourhood(self):
        self.update_neighbourhood()
        self.max_grievance = 0
        for n in self.neighbours:
            if n.type == "Police":
                continue
            if n.state == "Quiescent":
                continue
            if n.grievance > self.max_grievance:
                self.max_grievance = n.grievance
                if self.model.citizen_movement_type == 2:
                    n.update_citizen_neighbourhood_type_one()
                else:
                    n.update_citizen_neighbourhood_type_three()
                self.min_rioter = n.min_neighbour
                self.max_rioter = n

    def arrest_random(self): #arrests a random active citizen in sight and moves it to jail (off the grid)
        active_neighbours = []
        for agent in self.neighbours:
            if(
            agent.type == "Citizen"
            and agent.state == "Active"
            and agent.jail_sentence == 0
            ):
                active_neighbours.append(agent)
        self.arrestee_pos = (-1,-1)
        if active_neighbours:
            arrestee = self.random.choice(active_neighbours)
            sentence = self.random.randint(0, self.model.max_jail_sentence)
            arrestee.jail_sentence = sentence
            self.arrestee_pos = arrestee.pos
            self.model.grid._remove_agent(arrestee.pos,arrestee)
