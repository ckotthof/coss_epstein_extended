# Created 2020 by Group "Left as an Exercise to the Reader"
# for 851-0101-86L "Complex Social Systems: Modeling Agents, Learning, and Games"
# Semester: Winter 2020/2021
# ETH Zurich
#
# with help of Mesa framework
# based on Epstein's Civil Violence model

from mesa import Model

from mesa.space import SingleGrid
from mesa.time import RandomActivation
from mesa.datacollection import DataCollector


from .citizen import Citizen
from .policeman import PoliceMan
from typing import Tuple


Coordinate = Tuple[int, int]

def count_active(model): #method that counts the amount of currently active citizens on the grid
    active_count_var = 0
    for agent in model.schedule.agents:
        if agent.type == "Citizen":
            if agent.state == "Active" and agent.jail_sentence <= 0:
                active_count_var += 1

    return active_count_var

def count_prisoners(model): #method that counts the amount of current prisoners
    prisoner_count = 0
    for agent in model.schedule.agents:
        if agent.type == "Citizen":
            if agent.jail_sentence > 0:
                prisoner_count += 1
    return prisoner_count


class EpsteinModel(Model): #specifies the actual model that should be run


    def __init__(
    self,
    height = 40,
    width = 40,
    max_iters = 1000,
    citizen_density = 0.7,
    max_jail_sentence = 30,
    arrest_prob_constant = 2.3 ,
    active_threshold = 0.1,
    moore = False,
    legitimacy = 0.7,
    move_towards_same_hardship = True,
    police_movement_type = 0,
    citizen_movement_type = 0,
    random_threshold = 0.05,
    police_count = 40,
    strong_formation = False
    ):
        super().__init__()
        self.height = height
        self.width = width
        self.moore = moore
        self.schedule = RandomActivation(self)
        self.grid = SingleGrid(self.height, self.width, torus= True,)
        self.iteration = 0
        self.citizen_density = citizen_density
        self.max_jail_sentence = max_jail_sentence
        self.arrest_prob_constant = arrest_prob_constant
        self.active_threshold  = active_threshold
        self.max_iters = max_iters
        self.unique_id = 0
        self.legitimacy = legitimacy
        self.move_towards_same_hardship = move_towards_same_hardship
        self.police_movement_type = police_movement_type
        self.citizen_movement_type = citizen_movement_type
        self.random_threshold = random_threshold
        self.police_count = police_count
        self.strong_formation = strong_formation

        police_density = float(self.police_count) / (self.width * self.height)

        if police_density + self.citizen_density > 1:
            raise ValueError("Police density + citizen density must be less than 1")
        if police_count > self.width and self.police_movement_type == 2:
            raise ValueError("Police count must less than grid width")

        if police_movement_type < 2 or police_movement_type == 4: #where to place the policemen
            #in this case, place the police agents randomly on the grid
            for i in range( self.police_count):
                policeMan = PoliceMan(self.unique_id, self)
                self.schedule.add(policeMan)
                self.grid.position_agent(policeMan, "random", "random")
                self.unique_id += 1
        elif police_movement_type == 5:
            #place the police agents in an X symbol (this is for the diagonal movement.)
            for i in range(self.police_count):
                policeMan = PoliceMan(self.unique_id, self)
                self.schedule.add(policeMan)
                if self.unique_id % 2 == 0 :
                    self.grid.position_agent(policeMan, i, i)
                else:
                    self.grid.position_agent(policeMan,self.width-i, i-1)
                self.unique_id += 1
        else:
            #place all the police agents in the bottom row
            for i in range(self.police_count):
                policeMan = PoliceMan(self.unique_id, self)
                self.schedule.add(policeMan)
                self.grid.position_agent(policeMan, i, 0)
                self.unique_id += 1


        #fill the rest of the grid with the amount of citizens
        for (contents, x, y)in self.grid.coord_iter():
            if self.random.random() < self.citizen_density:
                if self.grid.is_cell_empty((x,y)):
                    citizen = Citizen(
                        self.unique_id,
                        self,
                        hardship=self.random.random(),
                        regime_legitimacy= self.legitimacy,
                        risk_aversion=self.random.random(),
                        threshold=self.active_threshold,
                        )
                    self.datacollector = DataCollector(
                        model_reporters= {
                            "Actives": count_active,
                            "Prisoners": count_prisoners,
                        },
                    )
                    citizen.pos= (x,y)
                    self.grid.position_agent(citizen, x, y)
                    self.schedule.add(citizen)
                    self.unique_id +=1
        self.running = True

    def step(self): #the step method is called in each iteration and defines what the model should do
        self.datacollector.collect(self) #amount of actives and prisoners are calculated
        self.schedule.step() #the scheduler will call the step method randomly on all the agents on the grid
        self.iteration +=1
        if self.iteration > self.max_iters:
            self.running = False
