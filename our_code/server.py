# Created 2020 by Group "Left as an Exercise to the Reader"
# for 851-0101-86L "Complex Social Systems: Modeling Agents, Learning, and Games"
# Semester: Winter 2020/2021
# ETH Zurich
#
# with help of Mesa framework
# based on Epstein's Civil Violence model

from mesa.visualization.ModularVisualization import ModularServer
from mesa.visualization.modules import CanvasGrid, ChartModule
from mesa.visualization.UserParam import UserSettableParameter

from .citizen import Citizen
from .policeman import PoliceMan
from our_code.model import EpsteinModel

#this file is really only needed when you want to visualize the model. 

POLICE_COLOR = "#000000"               #black
AGENT_QUIET_COLOR = "#0066CC"       #blue
AGENT_REBEL_COLOR = "#CC0000"       #red
JAIL_COLOR = "#757575"              #grey

AGENT_ETHNIC_1_QUIET_COLOR = "#008000" #Green
AGENT_REBEL_COLOR_1 = "#00FF44"        #Light Green

AGENT_ETHNIC_2_QUIET_COLOR = "#0066CC" #blue
AGENT_REBEL_COLOR_2 = "#00FFFB"        #lIGHT Blue

def citizen_police_portrayal(agent):
    #this function tells the webbrowser how to display individual agents (for each given agent). this function in particular is used for the upper graph, that shows actives, passives and cops.
    if agent is None:
        return

    portrayal = {
        "Shape": "circle",
        "x": agent.pos[0],
        "y": agent.pos[1],
        "Filled": "true",
        "Layer": 0,
    }

    if agent.type == "Citizen":
        portrayal["legitimacy"] = agent.regime_legitimacy
        portrayal["r"] = 0.8
        if agent.jail_sentence == 0:
            if agent.state == "Quiescent":
                portrayal["Color"] = AGENT_QUIET_COLOR
            else:
                portrayal["Color"] = AGENT_REBEL_COLOR
        else:
            portrayal["Color"] = JAIL_COLOR
    elif agent.type == "Police":
        portrayal["r"] = 0.5
        portrayal["Color"] = POLICE_COLOR
        portrayal["id"] = agent.unique_id
    return  portrayal

def grievance_rendering(agent):
    #this function is for the lower grid, which shows how aggrieved each agent is (and also shows cops)
    if agent is None:
        return
    portrayal = {
        "Shape": "rect",
        "x": agent.pos[0],
        "y": agent.pos[1],
        "Filled": "true",
        "w": 1,
        "h": 1,
        "Layer":0,
    }
    if agent.type == "Citizen":
        portrayal["Color"] = get_hex_colour(agent)
    elif agent.type == "Police":
        portrayal["Color"] = POLICE_COLOR
        portrayal["id"] = agent.unique_id
    return portrayal


def get_hex_colour(agent):
    red = 255
    green = 255 - int(255 * agent.grievance)
    blue = green
    return "#%02x%02x%02x" % (red, green, blue)



canvas_element = None
grievance_render = None
line_chart = ChartModule(
        [
            {"Label": "Actives", "Color": AGENT_REBEL_COLOR},
            {"Label": "Prisoners", "Color": JAIL_COLOR},
        ]
)

model_params = { #these are the parameters that the visual model gets initialized with first. because of "UserSettableParameter" the values can later be changed in the webbrowser.
    "move_towards_same_hardship": UserSettableParameter("checkbox", "Move towards ON=hardship, OFF=Grievance", True),
    "police_movement_type":UserSettableParameter("slider", "POLICE:0=Random,1=Towards_rioter,2=sweep_1,3=sweep_2,4=take_random_arrestees_pos,5=2x_diagonal",0, 0, 5, 1),
    "strong_formation": UserSettableParameter("checkbox", "force police to do strong line formation", False),
    "citizen_movement_type":UserSettableParameter("slider", "CITIZEN:0=Random,1=Towards_similar,2=Towards_similar_random.",0, 0, 2, 1),
    "legitimacy": UserSettableParameter ("slider", "Legitimacy: Global", 0.72, 0, 1, 0.01),
    "citizen_density":UserSettableParameter("slider", "Citizen Density", 0.7, 0, 1, 0.01),
    "police_count": UserSettableParameter("number", "Police Count", 40),
    "max_jail_sentence": UserSettableParameter ("number", "Maximum Jail Sentence", 30),
    "max_iters":UserSettableParameter("number", "maximum iterations",1000),
}

def serverstarter(height=40, width=40): #this function is called to start the actual server/webbrowser and interaction. it can be viewed as the main part that starts the whole thing
    model_params["height"]=height
    model_params["width"]=width

    canvas_element = CanvasGrid(citizen_police_portrayal, height, width , 480, 480)
    grievance_render = CanvasGrid(grievance_rendering, height, width , 480, 480)

    server = ModularServer(
        EpsteinModel, [canvas_element,grievance_render, line_chart], "Epstein Civil Violence", model_params
    )
    server.launch()
