# Created 2020 by Group "Left as an Exercise to the Reader"
# for 851-0101-86L "Complex Social Systems: Modeling Agents, Learning, and Games"
# Semester: Winter 2020/2021
# ETH Zurich
#
# with help of Mesa framework
# based on Epstein's Civil Violence model

from mesa import Agent



#superclass of all citizen and cops. defines a few methods that both of the classes frequently use.
class MovingAgent(Agent):

    """
    Class implementing the agents moving arount the grid

    The citizen and Cop agents inherit its methods

    """
    grid = None
    x = None
    y = None
    pos = None
    def __init__(
        self,
        unique_id,
        model,
    ):
        super().__init__(unique_id, model)

    def random_move(self):
        """
        Move to any cell within Vision
        """
        if self.empty_neighbours:
            new_pos = self.random.choice(self.empty_neighbours)
            self.model.grid.move_agent(self, new_pos)

    def swap_move(self, agent: Agent): #swaps postion with specified agent.
        victim_position = agent.pos
        if not (self.type=="Police" and self.model.strong_formation): #exception for police and to counter unforseeable scheduler ordering.
            if victim_position is not None:
                return
        agent.model.grid.remove_agent(agent) #remove the agent here
        current_agent_position = self.pos
        self.model.grid.move_agent(self, victim_position)
        self.model.grid.position_agent(agent, current_agent_position[0], current_agent_position[1]) #and put it back again to its new position


    def update_neighbourhood(self): #specifies neighbours that can be seen from the agents position and also where the empty spaces are
        self.neighbourhood = self.model.grid.get_neighborhood(self.pos, self.model.moore, radius = 2)
        self.neighbours = self.model.grid.get_cell_list_contents(self.neighbourhood)
        self.empty_neighbours = [
            c for c in self.neighbourhood if self.model.grid.is_cell_empty(c)
        ]
