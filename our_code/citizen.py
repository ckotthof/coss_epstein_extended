# Created 2020 by Group "Left as an Exercise to the Reader"
# for 851-0101-86L "Complex Social Systems: Modeling Agents, Learning, and Games"
# Semester: Winter 2020/2021
# ETH Zurich
#
# with help of Mesa framework
# based on Epstein's Civil Violence model

import math
import sys
from mesa import Agent
from our_code.moving_agent import MovingAgent

#Class of Citizens, that have internal variables to determine whether they are passive or active (or in jail). Citizens react to their environment as specified in the paper.
class Citizen(MovingAgent):

    def __init__(
        self,
        unique_id,
        model,
        hardship,
        regime_legitimacy,
        risk_aversion,
        threshold,
    ):
        super().__init__(unique_id, model)
        self.type = "Citizen"
        self.hardship = hardship
        self.regime_legitimacy = regime_legitimacy
        self.risk_aversion = risk_aversion
        self.threshold = threshold
        self.state = "Quiescent"
        self.jail_sentence = 0
        self.grievance = hardship * (1-regime_legitimacy)
        self.arrest_probability = None
        self.max_neighbour_list = None
        self.min_neighbour_list = None
        self.max_neighbour = None
        self.min_neighbour = None
        self.min_differences = None
        self.max_differences = None


    def step(self): #is called each iteration on each citizen
        if self.jail_sentence: #if in jail, deduct on unit from one's jail sentence
            self.jail_sentence -= 1
            if self.jail_sentence == 0: #if jail sentence is 0, the agent can return to the grid
                self.model.grid.move_to_empty(self)
            return

        if self.model.citizen_movement_type == 0:
            self.citizen_random_move()
        elif self.model.citizen_movement_type == 1: #this movement is an artifact, it was explored as a possibility to move to agents with the same level of grievance but was decided not to use in our paper.
            self.citizen_move_towards_similars()
        else: #this movement is an artifact, it was explored as a possibility to move to agents with the same level of grievance but was decided not to use in our paper.
            self.citizen_move_towards_similars_random()



    def citizen_random_move(self): #standard moving pattern for agents
        self.update_neighbourhood() #see what agents and empty places are near
        self.update_estimated_arrest_probability() #update the agents internal probability to be arrest on which further decision are based on
        net_risk = self.risk_aversion * self.arrest_probability 
        if self.state == "Quiescent" and (self.grievance - net_risk) > self.threshold:
            self.state = "Active" #if above threshold -> go active
        elif self.state == "Active" and (self.grievance - net_risk) <= self.threshold:
            self.state = "Quiescent" #if below or at threshold -> go passive


        self.random_move() #move to a random empty place

    def citizen_move_towards_similars(self): #this method is not used in the paper but models the citizens moving to other citizen that think the same
        self.update_citizen_neighbourhood_type_two()
        self.update_estimated_arrest_probability()
        net_risk = self.risk_aversion * self.arrest_probability

        if self.state == "Quiescent" and (self.grievance - net_risk) > self.threshold:
            self.state = "Active"
        elif self.state == "Active" and (self.grievance - net_risk) <= self.threshold:
            self.state = "Quiescent"
        self.move_towards_similar()

    def citizen_move_towards_similars_random(self): #this method is not used in the paper but models the citizens moving to other citizen that think the same
        self.update_citizen_neighbourhood_type_one()
        self.update_estimated_arrest_probability()
        net_risk = self.risk_aversion * self.arrest_probability

        if self.state == "Quiescent" and (self.grievance - net_risk) > self.threshold:
            self.state = "Active"
        elif self.state == "Active" and (self.grievance - net_risk) <= self.threshold:
            self.state = "Quiescent"
        self.move_towards_similar()

    def update_estimated_arrest_probability(self): 
        cops_in_vision = len([c for c in self.neighbours if c.type == "Police"]) #count how many cops the agent can see from its position
        actives_in_vision = 1.0  # citizen counts herself
        for c in self.neighbours:
            if (
                c.type == "Citizen"
                and c.state == "Active"
                and c.jail_sentence == 0
            ):
                actives_in_vision += 1  #count how many actives it can see
        self.arrest_probability = 1 - math.exp(
            -1 * self.model.arrest_prob_constant * (cops_in_vision / actives_in_vision) #calculate percieved arrest probability with epstein's formula
        ) 


    def update_citizen_neighbourhood_type_one(self): #this method would update variables for the citizen to see who thinks the same as the agent does.
        self.update_neighbourhood()
        self.max_neighbour = None
        self.min_neighbour = None
        self.max_differences = []
        self.min_differences = []
        self.max_neighbour_list = []
        self.min_neighbour_list = []

        for n in self.neighbours:
            if(n.type == "Police"):
                continue

            difference = 0 #not sure if python needs this for the scope...
            if self.model.move_towards_same_hardship:
                difference = abs(self.hardship - n.hardship)
            else:
                difference = abs(self.grievance - n.grievance)

            sorted_max_differences = sorted(self.max_differences)
            sorted_min_differences = sorted(self.min_differences)

            if len(self.max_neighbour_list) < 3:
                self.max_neighbour_list.append(n)
                self.max_differences.append(difference)
                self.max_neighbour = self.random.choice(self.max_neighbour_list)
            elif sorted_max_differences[2] is None or sorted_max_differences[2] < difference:
                sorted_max_differences[2] = difference
                self.max_neighbour_list[2] = n
                sorted_neighbours = []
                if self.model.move_towards_same_hardship:
                    sorted_neighbours = sorted(self.max_neighbour_list, key = lambda Citizen: abs(Citizen.hardship - self.hardship))
                else:
                    sorted_neighbours = sorted(self.max_neighbour_list, key = lambda Citizen: abs(Citizen.grievance - self.grievance))

                sorted_neighbours[2] = n
                self.max_neighbour = self.random.choice(self.max_neighbour_list)

            if len(self.min_neighbour_list) < 3:
                self.min_neighbour_list.append(n)
                self.min_differences.append(difference)
                self.min_neighbour = self.random.choice(self.min_neighbour_list)
            elif sorted_min_differences[2] > difference:
                sorted_min_differences[2] = difference

                sorted_neighbours = []
                if self.model.move_towards_same_hardship:
                    sorted_neighbours = sorted(self.min_neighbour_list, key = lambda Citizen: abs(Citizen.hardship - self.hardship))
                else:
                    sorted_neighbours = sorted(self.min_neighbour_list, key = lambda Citizen: abs(Citizen.grievance - self.grievance))

                sorted_neighbours[2] = n

                self.min_neighbour = self.random.choice(self.min_neighbour_list)


    def update_citizen_neighbourhood_type_two(self): #(another possibility) this method would update variables for the citizen to see who thinks the same as the agent does.
        self.update_neighbourhood()
        self.max_neighbour = None
        self.min_neighbour = None
        self.max_difference = 0
        self.min_difference = 0

        for n in self.neighbours:
            if(n.type == "Police"):
                continue

            if self.model.move_towards_same_hardship:
                difference = abs(self.hardship - n.hardship)
            else:
                difference = abs(self.grievance - n.grievance)

            if self.max_neighbour == None:
                self.max_neighbour = n
                self.max_difference = difference
            elif self.max_difference < difference:
                self.max_difference = difference
                self.max_neighbour = n

            if self.min_neighbour == None:
                self.min_neighbour = n
                self.min_difference = difference
            elif self.min_difference > difference:
                self.min_difference = difference
                self.min_neighbour = n

    def update_citizen_neighbourhood_type_three(self): #(yet another possibility) this method would update variables for the citizen to see who thinks the same as the agent does.
        self.update_neighbourhood()
        self.max_neighbour = None
        self.min_neighbour = None
        self.max_difference = 0
        self.min_difference = 0

        for n in self.neighbours:
            if(n.type == "Police"):
                continue

            if self.model.move_towards_same_hardship:
                difference = abs(self.hardship - n.hardship)
            else:
                difference = abs(self.grievance - n.grievance)

            if self.max_neighbour == None:
                self.max_neighbour = n
                self.max_difference = difference
            elif self.max_neighbour.grievance < n.grievance:
                self.max_difference = difference
                self.max_neighbour = n

            if self.min_neighbour == None:
                self.min_neighbour = n
                self.min_difference = difference
            elif self.min_neighbour.grievance > n.grievance:
                self.min_difference = difference
                self.min_neighbour = n

    def move_towards_similar(self): #this movement is an artifact, it was explored as a possibility to move to agents with the same level of grievance but was decided not to use in our paper.
        if self.model.citizen_movement_type == 2:
            self.update_citizen_neighbourhood_type_one()
            if self.min_neighbour is None:
                self.random_move()
                return
            self.min_neighbour.update_citizen_neighbourhood_type_one()
            if self.min_neighbour.empty_neighbours:
                new_pos = self.random.choice(self.min_neighbour.empty_neighbours)
                self.model.grid.move_agent(self, new_pos )
                return
        elif self.model.citizen_movement_type == 1:
            self.update_citizen_neighbourhood_type_two()
            if self.min_neighbour is None:
                self.random_move()
                return
            self.min_neighbour.update_citizen_neighbourhood_type_two()
            if self.min_neighbour.empty_neighbours:
                new_pos = self.random.choice(self.min_neighbour.empty_neighbours)
                self.model.grid.move_agent(self, new_pos )
                return
        if self.min_neighbour.max_neighbour is None or self.random.random() < self.model.random_threshold:

            self.random_move()
            return
        victim = self.min_neighbour.max_neighbour
        self.swap_move(victim)
