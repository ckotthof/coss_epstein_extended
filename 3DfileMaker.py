# Created 2020 by Group "Left as an Exercise to the Reader"
# for 851-0101-86L "Complex Social Systems: Modeling Agents, Learning, and Games"
# Semester: Winter 2020/2021
# ETH Zurich
#
# with help of Mesa framework
# based on Epstein's Civil Violence model

import pandas as pd
import glob
import os

#takes all run data the same police_movement_type and forced_line from the output folder and puts them next to each other in one big file that can later be viewed and plotted (in 3D) in excel

filenames = glob.glob("extracted_data/output/*.txt")

dfcollector = pd.DataFrame()
policemovement = filenames[0].split("pm")[1].split("_")[0]
print("making file for:")
print("pm"+policemovement)
forced = filenames[0].split("f")[1].split("_")[0]
print("f"+forced)

#filenames = glob.glob("output/pm"+policemovement+"_f"+forced+"_l*_cm*_js*.txt")

#print(filenames)

for file in filenames:
	legitimacy = file.split("l")[1].split("_")[0]
	df = pd.read_csv(file,delim_whitespace=True)
	dfcollector[legitimacy] = df["Actives"]

#print(dfcollector)


if not os.path.exists('extracted_data/3D_visuals_raw'):
	os.makedirs('extracted_data/3D_visuals_raw')

dfcollector.to_csv(r"extracted_data/3D_visuals_raw/3D_pm"+policemovement+"_f"+forced+".csv",  index=False, sep=';', mode='w')
