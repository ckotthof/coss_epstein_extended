import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import os
import sys
import pickle


if (len(sys.argv) != 4 and len(sys.argv) != 5 and len(sys.argv) != 6):
    print(
        "\nCall this program by 'python Mean_Plot.py <number_of_runs> <type> <police_movement_type> <forced_line> <police_movement_type2> <forced_line2> <police_movement_type3> <forced_line3> \nnumber of runs is the amount of runs you want to average over (3 for legitimacies, 4 for jail sentences) \ntype is j or l for jail sentence comparison or legitimacy comparison\npolice_movement_value is an int\nforced_line is 0 or 1\nthe second and third type of pm and f are optional and do not have to be filled")
    sys.exit()
else:
    n = sys.argv[1]
    n = int(n)
    t = sys.argv[2]
    i1 = sys.argv[3]
    if i1=="0":
        i1 = 0
    else:
        i1 = int(i1)
    mult = 1
    if (len(sys.argv) == 5 or len(sys.argv) == 6):
        i2 =  sys.argv[4]
        if i2=="0":
            i2 = 0
        else:
            i2 = int(i2)
        mult = 2
        if (len(sys.argv) == 6):
            i3 = sys.argv[5]
            if i3=="0":
                i3 = 0
            else:
                i3 = int(i3)
            mult = 3
    

if t == "l":
    legitimacies_arr1 = np.arange(0,40,5)
    legitimacies_arr2 = np.arange(40,55,1)
    legitimacies_arr3 = np.arange(55,90,0.25)
    legitimacies_arr = np.concatenate((legitimacies_arr1,legitimacies_arr2,legitimacies_arr3))
    max_jail_sentences = [ "45"]
    legitimacies = []
    for l in legitimacies_arr:
        legitimacies.append("%.4f" % ((l)/100))
elif t=="j":
    legitimacies = ["0.7000"]
    max_jail_sentences_arr = np.arange(0,200,2)
    max_jail_sentences = []
    for js in max_jail_sentences_arr:
        max_jail_sentences.append("%.2f" % (js))
    

indices_converter = np.array([["0","1","2","2","3","3","4","5","5"],#pms
                              ["0","0","0","1","0","1","0","0","1"]])#fs


colours = np.array(['#f5d838', '#ff5f16','#8B5DF3','#22055F','#2ed8ba','#093E6C', '#ff0f0f', '#bae672', '#067a00'])
#yellow, orange, light purple,dark purple, light turquoise, dark turquoise, red, light green, dark green
    

    
direc_out_comp = r'extracted_data/l_and_js_comparisons/'
if not os.path.exists('extracted_data/l_and_js_comparisons'):
    os.makedirs('extracted_data/l_and_js_comparisons')

def weighted_errors(data,std):
    #data as np.array
    #data_err is the standard error of the data as np.array
    #source for formulas: https://en.wikipedia.org/wiki/Weighted_arithmetic_mean
        w = 1/std**2
        mu = np.sum(data*w)/np.sum(w)
        sigma = np.sqrt(1/np.sum(w))
        return mu,sigma
    
ls = len(legitimacies)
js =len(max_jail_sentences)
mu_mat = np.zeros((ls,js,n))
sigma_mat = np.zeros((ls,js,n))
mu_arr = np.zeros((ls,js))
mu_err = np.zeros((ls,js))
pm = indices_converter[0,i1]
f = indices_converter[1,i1]

for r in range(n):
    if t == "j":
        mu_mat[:,:,r] = pickle.load( open( "extracted_data/pickled_data/mean_pm"+pm+"_f"+f+"_run"+str(r+1)+"_js.p", "rb" ))
        sigma_mat[:,:,r] = pickle.load( open( "extracted_data/pickled_data/std_pm"+pm+"_f"+f+"_run"+str(r+1)+"_js.p", "rb" ))
    else:
        mu_mat[:,:,r] = pickle.load( open( "extracted_data/pickled_data/mean_pm"+pm+"_f"+f+"_run"+str(r+1)+".p", "rb" ))
        sigma_mat[:,:,r] = pickle.load( open( "extracted_data/pickled_data/std_pm"+pm+"_f"+f+"_run"+str(r+1)+".p", "rb" ))
for i in range(ls):
    for j in range(js):
        mu_arr[i,j],mu_err[i,j] = weighted_errors(mu_mat[i,j,:],sigma_mat[i,j,:])

if mult >= 2:
    mu_mat2 = np.zeros((ls,js,n))
    sigma_mat2 = np.zeros((ls,js,n))
    mu_arr2 = np.zeros((ls,js))
    mu_err2 = np.zeros((ls,js))
    pm2 = indices_converter[0,i2]
    f2= indices_converter[1,i2]
    for r in range(n):
        if t == "j":
            mu_mat2[:,:,r] = pickle.load( open( "extracted_data/pickled_data/mean_pm"+pm2+"_f"+f2+"_run"+str(r+1)+"_js.p", "rb" ))
            sigma_mat2[:,:,r] = pickle.load( open( "extracted_data/pickled_data/std_pm"+pm2+"_f"+f2+"_run"+str(r+1)+"_js.p", "rb" ))
        else:
            mu_mat2[:,:,r] = pickle.load( open( "extracted_data/pickled_data/mean_pm"+pm2+"_f"+f2+"_run"+str(r+1)+".p", "rb" ))
            sigma_mat2[:,:,r] = pickle.load( open( "extracted_data/pickled_data/std_pm"+pm2+"_f"+f2+"_run"+str(r+1)+".p", "rb" ))
    for i in range(ls):
        for j in range(js):
            mu_arr2[i,j],mu_err2[i,j] = weighted_errors(mu_mat2[i,j,:],sigma_mat2[i,j,:])
    
    if mult ==3:
        mu_mat3 = np.zeros((ls,js,n))
        sigma_mat3 = np.zeros((ls,js,n))
        mu_arr3 = np.zeros((ls,js))
        mu_err3 = np.zeros((ls,js))
        pm3 = indices_converter[0,i3]
        f3= indices_converter[1,i3]
        for r in range(n):
            if t == "j":
                mu_mat3[:,:,r] = pickle.load( open( "extracted_data/pickled_data/mean_pm"+pm3+"_f"+f3+"_run"+str(r+1)+"_js.p", "rb" ))
                sigma_mat3[:,:,r] = pickle.load( open( "extracted_data/pickled_data/std_pm"+pm3+"_f"+f3+"_run"+str(r+1)+"_js.p", "rb" ))
            else:
                mu_mat3[:,:,r] = pickle.load( open( "extracted_data/pickled_data/mean_pm"+pm3+"_f"+f3+"_run"+str(r+1)+".p", "rb" ))
                sigma_mat3[:,:,r] = pickle.load( open( "extracted_data/pickled_data/std_pm"+pm3+"_f"+f3+"_run"+str(r+1)+".p", "rb" ))
        for i in range(ls):
            for j in range(js):
                mu_arr3[i,j],mu_err3[i,j] = weighted_errors(mu_mat3[i,j,:],sigma_mat3[i,j,:])
            



        


def legitimacy(js_it,js,fit=True):
    if fit == True:
        fit_gen = lambda x,a,b,c,d: a*(np.abs(x-b))**c+d
        p0s = np.zeros((4,mult))
        p0s = np.array([[-7.89488381,1.00099478,-0.70013112,39.1336913], #00 - 0
                    [-5.6663478 ,1.01327158,-0.77346424,30.42174889],  #10 - 1
                    [69.89381554,0.8838125,0.24053675,-17.80458923], #20 - 2
                    [-0.78165958,1.19178706,-3.28609192,45.33565976], #21 - 3
                    [-10.21823238,1.00690411,-0.62644417,41.840622], #30 - 4
                    [-0.98437074,1.16263277,-2.67848747,35.75941122], #31 - 5
                    [-6.32174063,0.99890583,-0.69310782,31.75332398], #40 - 6
                    [ 64.75194666,0.8911804,-0.13727204,-10.85925288], #50 - 7
                    [-0.92686025,1.19874112,-2.81073937,27.95034422]]) #51 - 8
        Param_gen,CovMat_gen = curve_fit(fit_gen,np.array(legitimacies).astype(np.float),mu_arr[:,js_it], p0 = p0s[i1,:])
        fitted_l = np.linspace(0,0.9,100)
        fitted_mu_gen = fit_gen(fitted_l,Param_gen[0],Param_gen[1],Param_gen[2],Param_gen[3])
        print(Param_gen)
    
    plt.clf()
    plt.figure(figsize=(12,8))
    plt.errorbar(np.array(legitimacies).astype(np.float),mu_arr[:,js_it],yerr=mu_err[:,js_it], fmt = 'o',c = colours[i1], capsize = 1, ms = 2.5, label = "pm" + pm + "f" + f)
    if fit == True:
        plt.plot(fitted_l,fitted_mu_gen,'-',c = colours[i1])

    if mult >= 2:
        plt.errorbar(np.array(legitimacies).astype(np.float),mu_arr2[:,js_it],yerr=mu_err2[:,js_it], fmt = 'o', c = colours[i2],capsize = 1, ms = 2.5, label = "pm" + pm2 + "f" + f2 )
        if fit == True:
            Param_gen,CovMat_gen = curve_fit(fit_gen,np.array(legitimacies).astype(np.float),mu_arr2[:,js_it], p0 = p0s[i2,:])
            fitted_mu_gen = fit_gen(fitted_l,Param_gen[0],Param_gen[1],Param_gen[2],Param_gen[3])
            print(Param_gen)
            plt.plot(fitted_l,fitted_mu_gen,'-', c = colours[i2])
        if mult == 3:
            plt.errorbar(np.array(legitimacies).astype(np.float),mu_arr3[:,js_it],yerr=mu_err3[:,js_it], fmt = 'o', c = colours[i3],capsize = 1, ms = 2.5, label = "pm" + pm3 + "f" + f3 )
            if fit==True:
                Param_gen,CovMat_gen = curve_fit(fit_gen,np.array(legitimacies).astype(np.float),mu_arr3[:,js_it], p0 = p0s[i3,:])
                fitted_mu_gen = fit_gen(fitted_l,Param_gen[0],Param_gen[1],Param_gen[2],Param_gen[3])
                print(Param_gen)
                plt.plot(fitted_l,fitted_mu_gen,'-', c = colours[i3])

    plt.ylabel("Mean Percentage of Actives")
    plt.xlabel("Legitimacies")
    filename_comp_jpeg = "compare_l_pm" + pm + "_f" + f + "_js" + js+ ".jpeg"
    if mult >= 2:
        #plt.legend()
        if mult == 2:
            filename_comp_jpeg = "compare_l_pm" + pm + "_f" + f + "_pm" + pm2 + "_f" + f2 + "_js" + js+ ".jpeg"
        else:
            filename_comp_jpeg = "compare_l_pm" + pm + "_f" + f + "_pm" + pm2 + "_f" + f2 + "_pm" + pm3 + "_f" + f3 +"_js" + js+ ".jpeg"
    full_path_comp_jpeg = os.path.join(direc_out_comp, filename_comp_jpeg)
    plt.savefig(full_path_comp_jpeg) 
    plt.show()
    

    
def jail_sentence(l_it,l,fit=True):
    p0s = np.array([[4.57088800e+01,-5.06309674e-02,-2.50712032e-02,5.88089816], #00 - 0
                    [18.57455425,-0.95013008,-0.0312696,5.08412359],  #10 - 1
                    [ 4.48918888e+01,-2.16496837e-01,-1.70710471e-02,2.48602453], #20 - 2
                    [ 7.11850041e+01,4.49854525e-01,-1.41908508e-02 ,1.37494878e+01], #21 - 3
                    [ 4.69715426e+01,-5.43152598e-02,-2.64195089e-02,5.40715706], #30 - 4
                    [ 2.57785742e+01,-5.86698148e-01,-1.94781257e-02,9.28353934], #31 - 5
                    [ 8.18139950e+01,4.82918494e-01,-3.34144321e-02,5.98678106], #40 - 6
                    [ 6.30910104e+01,1.09555364e-01,-1.91494941e-02 ,2.50305983], #50 - 7
                    [ 5.34638151e+01,1.34556284e-01,-2.55306977e-02,6.84217898]]) #51 - 8
    
    fit_exp = lambda x,a,b,c,d: a*(np.exp(c*x-b))+d
    Param_exp,CovMat_exp = curve_fit(fit_exp,np.array(max_jail_sentences).astype(np.float),mu_arr[l_it,:],  p0 = p0s[i1,:])
    fitted_l = np.linspace(0,200,200)
    fitted_mu_exp = fit_exp(fitted_l,Param_exp[0],Param_exp[1],Param_exp[2],Param_exp[3])
    print(Param_exp)
    
    plt.clf()
    plt.figure(figsize=(12,8))
    if fit==True:
        plt.plot(fitted_l,fitted_mu_exp,'-', c = colours[i1])
    plt.errorbar(max_jail_sentences_arr.astype(np.float),mu_arr[l_it,:],yerr=mu_err[l_it,:], fmt = 'o', c = colours[i1], capsize = 1, ms = 2.5, label = "pm" + pm + "f" + f )
    if mult >= 2:
        plt.errorbar(max_jail_sentences_arr.astype(np.float),mu_arr2[l_it,:],yerr=mu_err2[l_it,:], fmt = 'o', c = colours[i2],capsize = 1, ms = 2.5, label = "pm" + pm2 + "f" + f2 )
        if fit == True:
            Param_exp,CovMat_exp = curve_fit(fit_exp,np.array(max_jail_sentences).astype(np.float),mu_arr2[l_it,:], p0 = p0s[i2,:])
            fitted_mu_exp = fit_exp(fitted_l,Param_exp[0],Param_exp[1],Param_exp[2],Param_exp[3])
            print(Param_exp)
            plt.plot(fitted_l,fitted_mu_exp,'-', c = colours[i2])
        if mult == 3:
            plt.errorbar(max_jail_sentences_arr.astype(np.float),mu_arr3[l_it,:],yerr=mu_err3[l_it,:], fmt = 'o', c = colours[i3],capsize = 1, ms = 2.5, label = "pm" + pm3 + "f" + f3 )
            if fit==True:
                Param_exp,CovMat_exp = curve_fit(fit_exp,np.array(max_jail_sentences).astype(np.float),mu_arr3[l_it,:], p0 = p0s[i3,:])
                fitted_mu_exp = fit_exp(fitted_l,Param_exp[0],Param_exp[1],Param_exp[2],Param_exp[3])
                print(Param_exp)
                plt.plot(fitted_l,fitted_mu_exp,'-', c = colours[i3])
    plt.ylabel("Mean Percentage of Actives")
    plt.xlabel("Max Jail Sentences")
    filename_comp_jpeg = "compare_js_pm" + pm + "_f" + f + "_l" + l+ ".jpeg"
    if mult >= 2:
        #plt.legend()
        if mult == 2:
            filename_comp_jpeg = "compare_js_pm" + pm + "_f" + f + "_pm" + pm2 + "_f" + f2 + "_l" + l+ ".jpeg"
        else:
            filename_comp_jpeg = "compare_js_pm" + pm + "_f" + f + "_pm" + pm2 + "_f" + f2 + "_pm" + pm3 + "_f" + f3 +"_l" + l+ ".jpeg"
    full_path_comp_jpeg = os.path.join(direc_out_comp, filename_comp_jpeg)
    plt.savefig(full_path_comp_jpeg)    
    plt.show()
    
    
l_it = 0
js_it = 0

if t == "l":
    for js in max_jail_sentences:
        legitimacy(js_it,js)
        js_it +=1
else:
    for l in legitimacies:
        jail_sentence(l_it,l)
        l_it +=1
    


    
    
    
    
