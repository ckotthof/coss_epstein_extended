import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import os
import sys
import pickle
from uncertainties import ufloat

fit = True
raw= True

    
indices_converter = np.array([["0","1","2","2","3","3","4","5","5"],#pms
                              ["0","0","0","1","0","1","0","0","1"]])#fs
def weighted_errors(data,std):
    #data as np.array
    #data_err is the standard error of the data as np.array
    #source for formulas: https://en.wikipedia.org/wiki/Weighted_arithmetic_mean
        w = 1/std**2
        mu = np.sum(data*w)/np.sum(w)
        sigma = np.sqrt(1/np.sum(w))
        return mu,sigma


direc_out_comp = r'extracted_data/l_and_js_comparisons/plot_all/'
if not os.path.exists('extracted_data/l_and_js_comparisons/plot_all'):
    os.makedirs('extracted_data/l_and_js_comparisons/plot_all')



colours = np.array(['#f5d838', '#ff5f16','#8B5DF3','#22055F','#2ed8ba','#093E6C', '#ff0f0f', '#bae672', '#067a00'])
#yellow, orange, light purple,dark purple, light turquoise, dark turquoise, red, light green, dark green




"""
Legitimacies Plot
"""

legitimacies_arr1 = np.arange(0,40,5)
legitimacies_arr2 = np.arange(40,55,1)
legitimacies_arr3 = np.arange(55,90,0.25)
legitimacies_arr = np.concatenate((legitimacies_arr1,legitimacies_arr2,legitimacies_arr3))
max_jail_sentences = [ "45"]
legitimacies = []
for l in legitimacies_arr:
    legitimacies.append("%.4f" % ((l)/100))
        
ls = len(legitimacies)
js =len(max_jail_sentences)
ls = len(legitimacies)
js =len(max_jail_sentences)
mu_mat = np.zeros((ls,js,4,9)) #leg, jail, runs, pms
sigma_mat = np.zeros((ls,js,4,9))
mu_arr = np.zeros((ls,js,9))
mu_err = np.zeros((ls,js,9))
for i in range(9):
    for r in range(4):
        mu_mat[:,:,r,i] = pickle.load( open( "extracted_data/pickled_data/mean_pm"+indices_converter[0,i]+"_f"+indices_converter[1,i]+"_run"+str(r+1)+".p", "rb" ))
        sigma_mat[:,:,r,i] = pickle.load( open( "extracted_data/pickled_data/std_pm"+indices_converter[0,i]+"_f"+indices_converter[1,i]+"_run"+str(r+1)+".p", "rb" ))
        

for p in range(9):
    for i in range(ls):
        for j in range(js):
            mu_arr[i,j,p],mu_err[i,j,p] = weighted_errors(mu_mat[i,j,:,p],sigma_mat[i,j,:,p])
            

if fit == True:
    fit_gen = lambda x,a,b,c,d: a*(np.abs(x-b))**c+d
    p0s = np.array([[-7.89488381,1.00099478,-0.70013112,39.1336913], #00 - 0
                    [-5.6663478 ,1.01327158,-0.77346424,30.42174889],  #10 - 1
                    [69.89381554,0.8838125,0.24053675,-17.80458923], #20 - 2
                    [-0.78165958,1.19178706,-3.28609192,45.33565976], #21 - 3
                    [-10.21823238,1.00690411,-0.62644417,41.840622], #30 - 4
                    [-0.98437074,1.16263277,-2.67848747,35.75941122], #31 - 5
                    [-6.32174063,0.99890583,-0.69310782,31.75332398], #40 - 6
                    [ 64.75194666,0.8911804,-0.13727204,-10.85925288], #50 - 7
                    [-0.92686025,1.19874112,-2.81073937,27.95034422]]) #51 - 8
    Param = np.zeros((4,9))
    for i in range(9):
        Param[:,i],CovMat = curve_fit(fit_gen,np.array(legitimacies).astype(np.float),mu_arr[:,0,i], p0 = p0s[i,:])
        print("ppm: " + str(i))
        if CovMat[0,0]>0:
            k1 = ufloat(Param[0,i],CovMat[0,0])
            print('k1 {:.2u}'.format(k1))
        if CovMat[1,1]>0:
            k2 = ufloat(Param[1,i],CovMat[1,1])
            print('k2 {:.2u}'.format(k2))
        if CovMat[2,2]>0:
            k3 = ufloat(Param[2,i],CovMat[2,2])
            print('k3 {:.2u}'.format(k3))
        if CovMat[3,3]>0:
            k4 = ufloat(Param[3,i],CovMat[3,3])
            print('k4 {:.2u}'.format(k4))

    fitted_l = np.linspace(0,0.9,100)
    fitted_mu = np.zeros((100,9))
    for i in range(9):
        fitted_mu[:,i] = fit_gen(fitted_l,Param[0,i],Param[1,i],Param[2,i],Param[3,i])

plt.clf()
plt.figure(figsize=(12,8))
if raw == True:
    for i in range(9):
            plt.errorbar(np.array(legitimacies).astype(np.float),mu_arr[:,0,i],yerr=mu_err[:,0,i], fmt = 'o',c = colours[i], capsize = 1, ms = 2.5)
if fit == True:
    for i in range(9):
        plt.plot(fitted_l,fitted_mu[:,i],'-',c = colours[i])

plt.ylabel("Mean Percentage of Actives")
plt.xlabel("Legitimacies")
filename_comp_jpeg = "compare_l_all.eps"
full_path_comp_jpeg = os.path.join(direc_out_comp, filename_comp_jpeg)
plt.savefig(full_path_comp_jpeg) 
plt.show()




    

"""
Jail Sentences Plot
"""

legitimacies = ["0.7000"]
max_jail_sentences_arr = np.arange(0,200,2)
max_jail_sentences = []
for js in max_jail_sentences_arr:
    max_jail_sentences.append("%.2f" % (js))
        
ls = len(legitimacies)
js =len(max_jail_sentences)
ls = len(legitimacies)
js =len(max_jail_sentences)
mu_mat = np.zeros((ls,js,4,9)) #leg, jail, runs, pms
sigma_mat = np.zeros((ls,js,4,9))
mu_arr = np.zeros((ls,js,9))
mu_err = np.zeros((ls,js,9))
for i in range(9):
    for r in range(4):
        mu_mat[:,:,r,i] = pickle.load( open( "extracted_data/pickled_data/mean_pm"+indices_converter[0,i]+"_f"+indices_converter[1,i]+"_run"+str(r+1)+"_js.p", "rb" ))
        sigma_mat[:,:,r,i] = pickle.load( open( "extracted_data/pickled_data/std_pm"+indices_converter[0,i]+"_f"+indices_converter[1,i]+"_run"+str(r+1)+"_js.p", "rb" ))
        

for p in range(9):
    for i in range(ls):
        for j in range(js):
            mu_arr[i,j,p],mu_err[i,j,p] = weighted_errors(mu_mat[i,j,:,p],sigma_mat[i,j,:,p])
            

if fit == True:
    p0s = np.array([[4.57088800e+01,-5.06309674e-02,-2.50712032e-02,5.88089816], #00 - 0
                   [18.57455425,-0.95013008,-0.0312696,5.08412359],  #10 - 1
                   [ 4.48918888e+01,-2.16496837e-01,-1.70710471e-02,2.48602453], #20 - 2
                   [ 7.11850041e+01,4.49854525e-01,-1.41908508e-02 ,1.37494878e+01], #21 - 3
                   [ 4.69715426e+01,-5.43152598e-02,-2.64195089e-02,5.40715706], #30 - 4
                   [ 2.57785742e+01,-5.86698148e-01,-1.94781257e-02,9.28353934], #31 - 5
                   [ 8.18139950e+01,4.82918494e-01,-3.34144321e-02,5.98678106], #40 - 6
                   [ 6.30910104e+01,1.09555364e-01,-1.91494941e-02 ,2.50305983], #50 - 7
                   [ 5.34638151e+01,1.34556284e-01,-2.55306977e-02,6.84217898]]) #51 - 8
    fit_exp = lambda x,a,b,c,d: a*(np.exp(c*x-b))+d
    Param = np.zeros((4,9))
    for i in range(9):
        Param[:,i],CovMat= curve_fit(fit_exp,np.array(max_jail_sentences).astype(np.float),mu_arr[0,:,i],  p0 = p0s[i,:])
        print("pm: " + str(i))
        k1 = ufloat(Param[0,i],abs(CovMat[0,0]))
        print('k1 {:.2u}'.format(k1))
        k_temp = ufloat(Param[1,i],abs(CovMat[1,1]))
        k2 = ufloat(Param[2,i],CovMat[2,2])
        print('k2 {:.2u}'.format(k2))
        k3 = k_temp/k2
        print('k3 {:.2u}'.format(k3))
        k4 = ufloat(Param[3,i],CovMat[3,3])
        print('k4 {:.2u}'.format(k4))
    fitted_l = np.linspace(0,200,200)
    fitted_mu = np.zeros((200,9))
    for i in range(9):
        fitted_mu[:,i] = fit_exp(fitted_l,Param[0,i],Param[1,i],Param[2,i],Param[3,i])

plt.clf()
plt.figure(figsize=(12,8))
if raw == True:
    for i in range(9):
            plt.errorbar(np.array(max_jail_sentences).astype(np.float),mu_arr[0,:,i],yerr=mu_err[0,:,i], fmt = 'o',c = colours[i], capsize = 1, ms = 2.5)
if fit == True:
    for i in range(9):
        plt.plot(fitted_l,fitted_mu[:,i],'-',c = colours[i])

plt.ylabel("Mean Percentage of Actives")
plt.xlabel("Maximum Jail Sentences")
filename_comp_jpeg = "compare_j_all.eps"
full_path_comp_jpeg = os.path.join(direc_out_comp, filename_comp_jpeg)
plt.savefig(full_path_comp_jpeg) 
plt.show()
