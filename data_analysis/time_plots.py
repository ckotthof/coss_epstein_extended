import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import sys
import os
import pandas as pd

if (len(sys.argv) != 5 and len(sys.argv) != 7 and len(sys.argv) != 3):
    print(
        "\nCall this program by 'python Plots.py <t> <c> <police_movement_type> <forced_line> <police_movement_type2> <forced_line2>'\npolice_movement_value is an int\nforced_line is 1 or 0\nt is a (plot only actives) or p (prisoners) or s (both)\nc (cumulative) is True or False\npm2 and f2 are optional")
    exit()
elif (len(sys.argv)==3):
    t = sys.argv[1]
    c = sys.argv[2] #don't use this option
    plot_all = True
    mult=False
elif (len(sys.argv) == 5):
    t = sys.argv[1]
    c = sys.argv[2]
    pm = sys.argv[3]
    f = sys.argv[4]
    pm2 = "nan"
    f2 = "nan"
    mult = False #bool that tells the plot to plot two sets
    plot_all = False
else:
    t = sys.argv[1]
    c = sys.argv[2]
    pm = sys.argv[3]
    f = sys.argv[4]
    pm2 = sys.argv[5]
    f2 = sys.argv[6]
    mult = True
    plot_all = False
 
"""
legitimacies_arr = np.arange(0,90,10)
legitimacies = []
for l in legitimacies_arr:
    legitimacies.append("%.4f" % ((l)/100))    
max_jail_sentences = [ "45"]
"""

legitimacies = np.array(["0.7000"])
max_jail_sentences_arr = np.arange(0,200,40)
max_jail_sentences = []
for js in max_jail_sentences_arr:
    max_jail_sentences.append("%.0f" % (js))


indices_converter = np.array([["0","1","2","2","3","3","4","5","5"],#pms
                              ["0","0","0","1","0","1","0","0","1"]])#fs

colours = np.array(['#f5d838', '#ff5f16','#8B5DF3','#22055F','#2ed8ba','#093E6C', '#ff0f0f', '#bae672', '#067a00'])
#yellow, orange, light purple,dark purple, light turquoise, dark turquoise, red, light green, dark green
    
    
citizen_movements = ["0"]
direc_in = r'extracted_data/output/'
if plot_all == True:
    direc_out = r'extracted_data/graphs/graphs_plot_all/'
    if not os.path.exists('extracted_data/graphs/graphs_plot_all'):
        os.makedirs('extracted_data/graphs/graphs_plot_all')
elif mult == True:
    direc_out = r'extracted_data/graphs/graphs_pm'+pm+f+'_'+pm2+f2+'/'
    if not os.path.exists('extracted_data/graphs/graphs_pm'+pm+f+'_'+pm2+f2):
        os.makedirs('extracted_data/graphs/graphs_pm'+pm+f+'_'+pm2+f2)

else:
    direc_out = r'extracted_data/graphs/graphs_pm'+pm+f+'/'
    if not os.path.exists('extracted_data/graphs/graphs_pm'+pm+f):
        os.makedirs('extracted_data/graphs/graphs_pm'+pm+f)



def plot(actives, prisoners, actives2, prisoners2, pm, pm2, f, f2, l, cm, js, t, c,steps,plot_all, show=False):
    # t is type: "a" -> plot only actives ,"p" -> plot only prisoners
    # "s" -> plot both on same graph
    # c = True:  Plot cumulative, False: Plot raw data
    plt.close()
    plt.figure(figsize=(40,12))

    if t == "a":
        y_data = actives/11.20 #1120 for scaling to a percentage 
        if mult==True:
            y_data2 = actives2/11.20 #1120 for scaling 
    elif t == "p":
        y_data = prisoners/11.20 #1120 for scaling 
        if mult==True:
            y_data2 = prisoners2/11.20 #1120 for scaling 
    elif t == "s":
        y_data_1 = actives/11.20 #1120 for scaling 
        y_data_2 = prisoners/11.20 #1120 for scaling 
        if mult==True:
            y_data2_1 = actives2/11.20 #1120 for scaling 
            y_data2_2 = prisoners2/11.20 #1120 for scaling 
    else:
        print("Error: t must be either a (actives),p (prisoners) or s (both)")

    if c == "True" and t != "s":
        y_data = np.cumsum(y_data)
        if mult==True:
            y_data2 = np.cumsum(y_data2)
        c = "1"
    elif c == "True" and t == "s":
        y_data_1 = np.cumsum(y_data_1)
        y_data_2 = np.cumsum(y_data_2)
        if mult==True:
            y_data2_1 = np.cumsum(y_data2_1)
            y_data2_2 = np.cumsum(y_data2_2)
        c="1"
    else:
        c="0"
    
    if plot_all==False:
        if t != "s":
            plt.plot(steps, y_data,c='#f5d838')
            if mult==True:
                plt.plot(steps, y_data2, '-',c = '#bae672')
        if t == "s":
            plt.plot(steps, y_data_1, '-', c = '#194f2f', label = "a"+pm+f)
            plt.plot(steps, y_data_2, '-', c = '#75000e', label = "p"+pm+f)
            if mult==True:
                plt.plot(steps, y_data2_1, '-', c = '#3db337', label = "a"+pm2+f2)
                plt.plot(steps, y_data2_2, '-', c = '#f55b38', label = "p"+pm2+f2)
    else:
        for i in range(9):
            if t != "s":
                plt.plot(steps, y_data[:,i],c = colours[i] )
            if t == "s":
                plt.plot(steps, y_data_1[:,i], '-', c = '#194f2f')
                plt.plot(steps, y_data_2[:,i], '-', c = '#75000e')
                    
            
            
    plt.xlabel("Step",fontsize = 22)
    plt.ylabel("Percentage of Actives",fontsize=22)
    full_l = "%.4f" % (float(l)) #so that l=0.5 is saved as l=0.5000
    if mult == True:
        filename_jpeg = "pm" + pm + pm2 + "_f" + f + f2+ "_l" + full_l + "_cm" + cm + "_js" + js + "_t" + t + "_c" + c+".eps"
        plt.legend()
    elif plot_all == True:
        filename_jpeg = "plot_all_l" + full_l + "_cm" + cm + "_js" + js + "_t" + t + "_c" + c+".jpeg"
    else:
        filename_jpeg = "pm" + pm + "_f" + f + "_l" + full_l + "_cm" + cm + "_js" + js + "_t" + t + "_c" + c+".jpeg"

    full_path_jpeg = os.path.join(direc_out, filename_jpeg)
    plt.savefig(full_path_jpeg)
    if show == True and plot_all ==False:
        print("pm" + pm + " f" + f + " l" + l + " cm" + cm +
              " js" + js + " t" + t + " c" + c)
        plt.show()

for l in legitimacies:
    for js in max_jail_sentences:
        for cm in citizen_movements:
            if l[-1] == "0": #This is because of the weird naming of the output files that writes 0.500 as 0.5
                l = l[:-1]
                if l[-1] == "0":
                    l = l[:-1]
                    if l[-1] == "0":
                        l = l[:-1]

            if plot_all ==True:
                actives  = np.zeros((4000,9))
                prisoners = np.zeros((4000,9))
                for i in range(9):
                    """
                    if indices_converter[0,i]=="1":
                        l_adj = "%.4f" % (float(l))
                    else:
                        l_adj = l
                    """
                    if indices_converter[0,i] =="1" or (indices_converter[0,i] =="5" and indices_converter[1,i]=="0"):
                       l_adj = "0.70"
                    else:
                        l_adj  = "0.7"
                    
                    filename_txt = "pm" + indices_converter[0,i] + "_f" + indices_converter[1,i] + "_l" + l_adj + "_cm" + cm + "_js" + js + ".txt"
                    full_path_txt = os.path.join(direc_in, filename_txt)
                    data_pandas = pd.read_csv(full_path_txt, sep=' ', header=None, skiprows=1)
                    data = data_pandas.to_numpy()
                    actives[:,i] = data[:4000,0] 
                    prisoners[:,i] = data[:4000,1]
                data2 = np.zeros((0,2))
                pm = "err"
                pm2 = "err"
                f="err"
                f2 = "err"
                steps = np.arange(1, np.size(data[:,0])+1)
                plot(actives, prisoners, data2[:,0], data2[:,1], pm, pm2, f,f2, l, cm, js, t, c,steps,plot_all, show=False)

                
            else:  
                if pm2 =="1" or (pm2 =="5" and f2=="0"):
                   l = "0.70"
                """
                if pm=="1":
                    l = "%.4f" % (float(l))
                """
                filename_txt = "pm" + pm + "_f" + f + "_l" + "0.7" + "_cm" + cm + "_js" + js + ".txt"
                full_path_txt = os.path.join(direc_in, filename_txt)
                data_pandas = pd.read_csv(full_path_txt, sep=' ', header=None, skiprows=1)
                data = data_pandas.to_numpy()
                steps = np.arange(1, np.size(data[:,0])+1)
                if mult ==True:
                    filename_txt2 = "pm" + pm2 + "_f" + f2 + "_l" + l + "_cm" + cm + "_js" + js + ".txt"
                    full_path_txt2 = os.path.join(direc_in, filename_txt2)
                    data_pandas2 = pd.read_csv(full_path_txt2, sep=' ', header=None, skiprows=1)
                    data2 = data_pandas2.to_numpy()
                    if np.size(data[:,0]) != np.size(data2[:,0]):
                        print("Error, two data sets do not have the same number of steps.")
                        pass
                else:
                    data2 = np.zeros((0,2))
                plot(data[:, 0], data[:, 1], data2[:,0], data2[:,1], pm, pm2, f,f2, l, cm, js, t, c,steps,plot_all, show=False)
