# -*- coding: utf-8 -*-
"""
Created on Sun Nov 15 14:47:40 2020

@author: crkau
"""

import matplotlib.pyplot as plt
import numpy as np
import sys
import os
import pandas as pd
from scipy.optimize import curve_fit
from uncertainties import ufloat, unumpy
from uncertainties.umath import *


if (len(sys.argv) != 7) and (len(sys.argv) != 5):
    print(
        "\nCall this program by 'python linfitting.py <i_all> (<police_movement_type> <forced_line>) <runs> <var> <js/l>'\ni_all is True or False: automaticall generates linfit for all \npolice_movement_value is an int\nforced_line is 1 or 0\n runs is int: number of runs to average results over\nvar is 'js' or 'leg' varied parameter\njs/l is jail sentence length or legitimacy that is constant parameter\n")
    exit()
elif (len(sys.argv) == 7):
    i_all = sys.argv[1]
    pm = sys.argv[2]
    f = sys.argv[3]
    t = 'a'
    runs = int(sys.argv[4])
    plot = False
    var = sys.argv[5]
    js = sys.argv[6]
elif (len(sys.argv) == 5):
    i_all = sys.argv[1]
    t = 'a'
    plot = False
    runs = int(sys.argv[2])
    var = sys.argv[3]
    js = sys.argv[4]

if var == 'js':
    leg = js


if not (var == 'js' or var == 'leg' and runs >= 0):
    print(
        "\nInvalid Call\n")
    exit()

legitimacies_arr1 = np.arange(0,40,5)
legitimacies_arr2 = np.arange(40,55,1)
legitimacies_arr3 = np.arange(55,90,0.25)
legitimacies_arr = np.concatenate((legitimacies_arr1,legitimacies_arr2,legitimacies_arr3))
legitimacies = []
for ls in legitimacies_arr:
    legitimacies.append("%.4f" % ((ls)/100))
max_jail_sentences = np.arange(1, 100, 1)*2
citizen_movements = ["0"]
if var == 'js':
    direc_in = r'./output_js/'
    direc_out = r'./linear_js/'
if var == 'leg':
    direc_in = r'./output_leg/output_data/'
    direc_out = r'./linear_leg/'

if var == 'js':
    legitimacies = [str(leg)]
    legitimacies_arr = [float(leg)*100]
elif var == 'leg':
    max_jail_sentences = [str(js)]

pm_all = [0, 1, 2, 2, 3, 3, 4, 5, 5]
f_all = [0, 0, 0, 1, 0, 1, 0, 0, 1]

#Colors of different pm movement types
color00 = "#f5d838" #yellow
color10 = "#ff5f16" #orange
color20 = "#8B5DF3" #light violet
color21 = "#22055F" #dark violet
color30 = "#2ed8ba" #light turquoise
color31 = "#093E6C" #dark turquoise
color40 = "#ff0f0f" #red
color50 = "#bae672" #light green
color51 = "#067a00" #dark green

colors = [color00, color10, color20, color21, color30, color31, color40, color50, color51]
indices_color = [0, 1, 2, 4, 6, 7]

    
if not os.path.exists('linear_js'):
    os.makedirs('linear_js')
if not os.path.exists('linear_leg'):
    os.makedirs('linear_leg')

if i_all == 'False':    
    if t == "a":
        for n in np.arange(1, runs+1):
            if n == 1:
                filepath_lin = os.path.join(direc_out, "linfitparams_actives" + str(pm) + str(f) + ".txt")
            else:
                filepath_lin = os.path.join(direc_out, "linfitparams_actives" + str(pm) + str(f) + "_run" + str(n) + ".txt")
            file_a = open(filepath_lin, "w+")
            file_a.write('Linear Parameters Actives y = a + b*x \n \n')
            file_a.close()
    if t == "p":
        for n in np.arange(1, runs+1):
            if n == 1:
                filepath_lin = os.path.join(direc_out, "linfitparams_prisoners" + str(pm) + str(f) + ".txt")
            else:
                filepath_lin = os.path.join(direc_out, "linfitparams_prisoners" + str(pm) + str(f) + "_run" + str(n) + ".txt")
            file_p = open(filepath_lin, "w+")
            file_p.write('Linear Parameters Prisoners y = a + b*x \n \n')
            file_p.close()
    if t == "s":
        for n in np.arange(1, runs+1):
            if n == 1:
                filepath_lin = os.path.join(direc_out, "linfitparams" + str(pm) + str(f) + ".txt")
            else:
                filepath_lin = os.path.join(direc_out, "linfitparams" + str(pm) + str(f) + "_run" + str(n) + ".txt")
            file_p = open(filepath_lin, "w+")
            file_p.write('Linear Parameters y = a + b*x \n \n')
            file_p.close()
if i_all == 'True':
    if t == "a":
        for pm0, f0 in zip(pm_all, f_all):
            for n in np.arange(1, runs+1):
                if n == 1:
                    filepath_lin = os.path.join(direc_out, "linfitparams_actives" + str(pm0) + str(f0) + ".txt")
                else:
                    filepath_lin = os.path.join(direc_out, "linfitparams_actives" + str(pm0) + str(f0) + "_run" + str(n) + ".txt")
                file_a = open(filepath_lin, "w+")
                file_a.write('Linear Parameters Actives y = a + b*x \n \n')
                file_a.close()
    if t == "p":
        for pm0, f0 in zip(pm_all, f_all):
            for n in np.arange(1, runs+1):
                if n == 1:
                    filepath_lin = os.path.join(direc_out, "linfitparams_prisoners" + str(pm0) + str(f0) + ".txt")
                else:
                    filepath_lin = os.path.join(direc_out, "linfitparams_prisoners" + str(pm0) + str(f0) + "_run" + str(n) + ".txt")
                file_p = open(filepath_lin, "w+")
                file_p.write('Linear Parameters Prisoners y = a + b*x \n \n')
                file_p.close()
    if t == "s":
        for pm0, f0 in zip(pm_all, f_all):
            for n in np.arange(1, runs+1):
                if n == 1:
                    filepath_lin = os.path.join(direc_out, "linfitparams" + str(pm0) + str(f0) + ".txt")
                else:
                    filepath_lin = os.path.join(direc_out, "linfitparams" + str(pm0) + str(f0) + "_run" + str(n) + ".txt")
                file_p = open(filepath_lin, "w+")
                file_p.write('Linear Parameters y = a + b*x \n \n')
                file_p.close()
    
    
def linparam(actives, prisoners, pm, f,  l, cm, js, t, show=False):
    if t == "a":
        y_data = actives/1120 #1120 for scaling
        steps = np.arange(1, np.size(y_data) + 1)
    elif t == "p":
        y_data = prisoners/1120 #1120 for scaling
        steps = np.arange(1, np.size(y_data) + 1)
    elif t == "s":
        y_data_1 = actives/1120 #1120 for scaling 
        y_data_2 = prisoners/1120 #1120 for scaling 
        steps = np.arange(1, np.size(y_data_1) + 1)
    else:
        print("Error: t must be either a (actives),p (prisoners) or s (both)")
    
    
    
        
    lin_func = lambda x, a, b: a + b*x
    sigma = np.ones_like(steps)
    if pm == 5 and f == 0:
        sigma[:1000] *= 1000
    else:
        sigma[:100] *= 100
    
    
    if t == "a":
        param, covmat = curve_fit(lin_func, steps, y_data, sigma = sigma)
        b = ufloat(param[1], np.sqrt(covmat[1, 1]))
        a = float(np.size(y_data))/2*b + ufloat(param[0], np.sqrt(covmat[0,0]))
        params = np.array([a, b])
    elif t == "p":
        param, covmat = curve_fit(lin_func, steps, y_data, sigma = sigma)
        params = unumpy.uarray(param, np.sqrt(np.diag(covmat)))
    elif t == "s":
        param_a, covmat_a = curve_fit(lin_func, steps, y_data_1, sigma = sigma)
        param_p, covmat_p = curve_fit(lin_func, steps, y_data_2, sigma = sigma)
        params_a = unumpy.uarray(param_a, np.sqrt(np.diag(covmat_a)))
        params_p = unumpy.uarray(param_p, np.sqrt(np.diag(covmat_p)))
    
    if t == "a" or t == "p":
        return params
    if t == "s":
        return params_a, params_p
    
def linfit_txt(actives, prisoners, pm, f,  l, cm, js, t, run = 1, show=False):
    if t == "a":
        params = linparam(actives, prisoners, pm, f,  l, cm, js, t, show=False)
        if run == 1:
            filepath = os.path.join(direc_out, "linfitparams_actives" + str(pm) + str(f) + ".txt")
        else:
            filepath = os.path.join(direc_out, "linfitparams_actives" + str(pm) + str(f) + "_run" + str(run) + ".txt")
        file = open(filepath, 'a')
        textstr = '\n'.join((
                "l" + str(l),
                "js" + str(js),
                r'a = {:.2u}'.format(params[0]),
                r'b = {:.2u}'.format(params[1]),
                '\n'
                ))
        file.write(textstr)
        file.close()
    elif t == "p":
        params = linparam(actives, prisoners, pm, f,  l, cm, js, t, show=False)
        if run == 1:
            filepath = os.path.join(direc_out, "linfitparams_prisoners" + str(pm) + str(f) + ".txt")
        else:
            filepath = os.path.join(direc_out, "linfitparams_prisoners" + str(pm) + str(f) + "_run" + str(run) + ".txt")
        file = open(filepath, 'a')
        textstr = '\n'.join((
                "l" + str(l),
                "js" + str(js),
                r'a = {:.2u}'.format(params[0]),
                r'b = {:.2u}'.format(params[1]),
                '\n'
                ))
        file.write(textstr)
        file.close()
        
        
    elif t == "s":
        params_a, params_p = linparam(actives, prisoners, pm, f,  l, cm, js, t, show=False)
        if run == 1:
            filepath = os.path.join(direc_out, "linfitparams" + str(pm) + str(f) + ".txt")
        else:
            filepath = os.path.join(direc_out, "linfitparams" + str(pm) + str(f) + "_run" + str(run) + ".txt")
        file = open(filepath, 'a')
        textstr = '\n'.join((
                "l" + str(l),
                "js" + str(js),
                r'Parameters Actives:',
                r'a = {:.2u}'.format(params_a[0]),
                r'b = {:.2u}'.format(params_a[1]),
                r'Parameters Prisoners:',
                r'a = {:.2u}'.format(params_p[0]),
                r'b = {:.2u}'.format(params_p[1]),
                '\n'
                ))
        file.write(textstr)
        file.close()
    
    
def linfit_plot(actives, prisoners, pm, f,  l, cm, js, t, show=False):
    if t == "a":
        y_data = actives/1120 #1120 for scaling
        steps = np.arange(1, np.size(y_data) + 1)
    elif t == "p":
        y_data = prisoners/1120 #1120 for scaling 
        steps = np.arange(1, np.size(y_data) + 1)
    elif t == "s":
        y_data_1 = actives/1120 #1120 for scaling 
        y_data_2 = prisoners/1120 #1120 for scaling 
        steps = np.arange(1, np.size(y_data_1) + 1)
    else:
        print("Error: t must be either a (actives),p (prisoners) or s (both)")
    
    lin_func = lambda x, a, b: a + b*x
    plt.clf()

    if t == "a":
        params = linparam(actives, prisoners, pm, f,  l, cm, js, t, show=False)
        color_index = indices_color[int(pm)] + int(f)
        plt.plot(steps, y_data, 'o', color=colors[color_index], ms=1.5)
        plt.plot(steps, lin_func(steps, *unumpy.nominal_values(params)), linewidth=4)
    elif t == "p":
        params = linparam(actives, prisoners, pm, f,  l, cm, js, t, show=False)
        color_index = indices_color[int(pm)] + int(f)
        plt.plot(steps, y_data, 'o', color=colors[color_index], ms=1.5)
        plt.plot(steps, lin_func(steps, *unumpy.nominal_values(params)))
    elif t == "s":
        params_a, params_p = linparam(actives, prisoners, pm, f,  l, cm, js, t, show=False)
        plt.plot(steps, y_data_1, 'go', ms=1.5)
        plt.plot(steps, y_data_2, 'ro', ms=1.5)
        plt.plot(steps, lin_func(steps, *unumpy.nominal_values(params_a)), 'b-', linewidth=4)
        plt.plot(steps, lin_func(steps, *unumpy.nominal_values(params_p)), 'k-', linewidth=4)
    
    plt.vlines(int(np.size(y_data)/2), np.amin(y_data), np.amax(y_data), color = 'k', linestyles = 'dashed')
    plt.xlabel("Step")
    plt.ylabel("Ratio of Actives")
    filename_jpeg = "pm" + str(pm) + "_f" + str(f) + "_l" + str(l) + "_cm" + str(cm) + "_js" + str(js) + "_t" + str(t) + "linfitted.jpeg"
    full_path_jpeg = os.path.join(direc_out, filename_jpeg)
    plt.savefig(full_path_jpeg)
    if show == True:
        plt.show()
        
if runs == 1:
 
    if i_all == 'False':
        for l in legitimacies:
            for js in max_jail_sentences:
                for cm in citizen_movements:
                    if not int(pm) == 1:
                        if l[-1] == "0" and pm != 1:
                            l = l[:-1]
                        if l[-1] == "0" and pm != 1:
                            l = l[:-1]
                        if l[-1] == "0" and pm != 1:
                            l = l[:-1]
                    if var == 'js':
                        if pm0==1:
                            l = '0.70'
                        elif (pm0==5 and f==0):
                            l = '0.70'
                        elif (pm0==5 and f == 1 and (n==2 or n==3)):
                            l = '0.70'
                        else:
                            l = l
                    filename_txt = "pm" + str(pm) + "_f" + str(f) + "_l"  + str(l) + "_cm" + str(cm) + "_js" + str(js) + ".txt"
                    full_path_txt = os.path.join(direc_in, filename_txt)
                    data_pandas = pd.read_csv(full_path_txt, sep=' ', header=None, skiprows=1)
                    data = data_pandas.to_numpy()
                    linparam(data[:,0], data[:, 1], pm, f, l, cm, js, t, show=False)
                    linfit_txt(data[:,0], data[:, 1], pm, f, l, cm, js, t, show=False)
                    if plot:
                        linfit_plot(data[:,0], data[:, 1], pm, f, l, cm, js, t, show=False)
    elif i_all == 'True':
        for pm0, f0 in zip(pm_all, f_all):
            for l, ls in zip(legitimacies, legitimacies_arr):
                for js in max_jail_sentences:
                    for cm in citizen_movements:
                        if not int(pm0) == 1:
                            if l[-1] == "0" and pm0 != 1:
                                l = l[:-1]
                            if l[-1] == "0" and pm0 != 1:
                                l = l[:-1]
                            if l[-1] == "0" and pm0 != 1:
                                l = l[:-1]
                        if var == 'js':
                            if pm0==1:
                                l = '0.70'
                            elif (int(pm0)==5 and int(f)==0):
                                l = '0.70'
                            elif (int(pm0)==5 and int(f) == 1 and (n==2 or n==3)):
                                l = '0.70'
                            else:
                                l = l
                        filename_txt = "pm" + str(pm0) + "_f" + str(f0) + "_l" + str(l) + "_cm" + str(cm) + "_js" + str(js) + ".txt"
                        full_path_txt = os.path.join(direc_in, filename_txt)
                        data_pandas = pd.read_csv(full_path_txt, sep=' ', header=None, skiprows=1)
                        data = data_pandas.to_numpy()
                        linparam(data[:,0], data[:, 1], pm0, f0, ls/100, cm, js, t, show=False)
                        linfit_txt(data[:,0], data[:, 1], pm0, f0, ls/100, cm, js, t, show=False)
                        if plot:
                            linfit_plot(data[:,0], data[:, 1], pm0, f0, ls/100, cm, js, t, show=False)

if runs != 1:
    for n in np.arange(1, runs+1):
        if i_all == 'False':
            for l, ls in zip(legitimacies, legitimacies_arr):
                for js in max_jail_sentences:
                    for cm in citizen_movements:
                        if not int(pm) == 1:
                            if l[-1] == "0" and pm != 1:
                                l = l[:-1]
                            if l[-1] == "0" and pm != 1:
                                l = l[:-1]
                            if l[-1] == "0" and pm != 1:
                                l = l[:-1]
                        if var == 'js':
                            if pm==1:
                                l = '0.70'
                            elif (int(pm)==5 and int(f)==0):
                                l = '0.70'
                            elif (int(pm)==5 and int(f) == 1 and (n==2 or n==3)):
                                l = '0.70'
                            else:
                                l = l
                        print(l)
                        if n == 1:
                            filename_txt = "pm" + str(pm) + "_f" + str(f) + "_l"  + str(l) + "_cm" + str(cm) + "_js" + str(js) + ".txt"
#                        elif pm == 5 and f == 1 and (n == 2 or n == 3):
#                            filename_txt = "pm" + str(pm) + "_f" + str(f) + "_l"  + str(l) + "_cm" + str(cm) + "_js" + str(js) + ".txt"
                        else:
                            filename_txt = "pm" + str(pm) + "_f" + str(f) + "_l"  + str(l) + "_cm" + str(cm) + "_js" + str(js) + "_run" + str(n) + ".txt"
                        full_path_txt = os.path.join(direc_in, filename_txt)
                        data_pandas = pd.read_csv(full_path_txt, sep=' ', header=None, skiprows=1)
                        data = data_pandas.to_numpy()
                        linparam(data[:,0], data[:, 1], pm, f, ls/100, cm, js, t, show=False)
                        linfit_txt(data[:,0], data[:, 1], pm, f, ls/100, cm, js, t, run = n, show=False)
                        if plot:
                            linfit_plot(data[:,0], data[:, 1], pm, f, ls/100, cm, js, t, show=False)
        elif i_all == 'True':
            for pm0, f0 in zip(pm_all, f_all):
                for l, ls in zip(legitimacies, legitimacies_arr):
                    for js in max_jail_sentences:
                        for cm in citizen_movements:
                            if not int(pm0) == 1:
                                if l[-1] == "0" and pm0 != 1:
                                    l = l[:-1]
                                if l[-1] == "0" and pm0 != 1:
                                    l = l[:-1]
                                if l[-1] == "0" and pm0 != 1:
                                    l = l[:-1]
                            if var == 'js':
                                if pm0==1:
                                    l = '0.70'
                                elif (int(pm0)==5 and int(f0)==0):
                                    l = '0.70'
                                elif (int(pm0)==5 and int(f0) == 1 and (n==2 or n==3)):
                                    l = '0.70'
                                else:
                                    l = l
                            if n == 1:
                                filename_txt = "pm" + str(pm0) + "_f" + str(f0) + "_l"  + str(l) + "_cm" + str(cm) + "_js" + str(js) + ".txt"
#                            elif pm == 5  and f == 1 or f == 0 and (n == 2 or n == 3) or f == 0:
#                                filename_txt = "pm" + str(pm0) + "_f" + str(f0) + "_l"  + str(l) + "_cm" + str(cm) + "_js" + str(js) + ".txt"
                            else:
                                filename_txt = "pm" + str(pm0) + "_f" + str(f0) + "_l"  + str(l) + "_cm" + str(cm) + "_js" + str(js) + "_run" + str(n) + ".txt"
                            full_path_txt = os.path.join(direc_in, filename_txt)
                            data_pandas = pd.read_csv(full_path_txt, sep=' ', header=None, skiprows=1)
                            data = data_pandas.to_numpy()
                            linparam(data[:,0], data[:, 1], pm0, f0, ls/100, cm, js, t, show=False)
                            linfit_txt(data[:,0], data[:, 1], pm0, f0, ls/100, cm, js, t, run = n, show=False)
                            if plot:
                                linfit_plot(data[:,0], data[:, 1], pm0, f0, ls/100, cm, js, t, show=False)
        print('-------------------------------run ' + str(n) + ' done----------------------------------')
