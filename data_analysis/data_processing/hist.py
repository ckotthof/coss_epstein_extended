import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import sys
import os
import pandas as pd
import pickle

if (len(sys.argv) != 6):
    print(
        "\nCall this program by 'python Plots.py <police_movement_type> <forced_line> <number_of_steps> <run> <type>\npolice_movement_value is an int\nforced_line is 1\ntype is <l> for legitimacy comparison and  <j> for jail sentence comparison" )
    sys.exit()
elif (len(sys.argv) == 6):
    pm = sys.argv[1]
    f = sys.argv[2]
    s = sys.argv[3] #number of steps
    s = int(s)
    r = sys.argv[4]
    t = sys.argv[5]

    


steps = np.arange(1, s+1)

if t == "j":
    legitimacies = np.array(["0.7000"])
    max_jail_sentences_arr = np.arange(0,200,2)
    max_jail_sentences = []
    for js in max_jail_sentences_arr:
        max_jail_sentences.append("%.0f" % (js))
elif t == "l":
    legitimacies_arr1 = np.arange(0,40,5)
    legitimacies_arr2 = np.arange(40,55,1)
    legitimacies_arr3 = np.arange(55,90,0.25)
    legitimacies_arr = np.concatenate((legitimacies_arr1,legitimacies_arr2,legitimacies_arr3))
    legitimacies = []
    for l in legitimacies_arr:
        legitimacies.append("%.4f" % ((l)/100))
    max_jail_sentences = [ "45"]
else:
    print("Error: t must be either j or l")
    sys.exit()


citizen_movements = ["0"]
direc_in = r'extracted_data/output/'
direc_out_hist = r'extracted_data/histograms/histograms_pm'+pm+f+'/'
if not os.path.exists('extracted_data/histograms/histograms_pm'+pm+f):
    os.makedirs('extracted_data/histograms/histograms_pm'+pm+f)



mu_arr = np.zeros((len(legitimacies),len(max_jail_sentences))) #adjust if we wanna explore citizen movements again
sigma_arr = np.zeros((len(legitimacies),len(max_jail_sentences)))

if pm == "5" and f == "0":
    print("pm 5 f 0 has been adjusted not to take into account the first 1000 steps")
    s-=1000


def hist(actives, pm, f, l, cm, js,t, show=False, with_fit = True):
    if pm == "5" and f == "0" and t=="l":
        actives = actives[980:] #it sometimes takes 1000 steps for it to find an equilibrium
    elif t== "j":
        actives = actives[1980:] #it sometimes takes 2000 steps for it to find an equilibrium
    
    plt.close()
    plt.figure(figsize=(9,6))
    freq = np.bincount(actives) #this is for the normal distribution fit
    data = actives/11.20 #1120 for scaling to a percentage 
 
    #Gaussian Fit
    
    x_fit1 = np.arange(0,np.size(freq)*100/1120,100/1120)
    fit = lambda x, sigma, mu, amplitude: amplitude*np.exp(-(x-mu)**2/(2*sigma**2))
    
    if t == "l":
        if pm == "0":
            if float(l)<= 0.60:
                p0 = np.array([2,28,15*(s*1e-3)])
            elif float(l)<= 0.65:
                p0 = np.array([2,28,15*(s*1e-3)])
            elif float(l)<= 0.70:
                p0 = np.array([2,25,15*(s*1e-3)])
            elif float(l)<= 0.75:
                p0 = np.array([2,21,15*(s*1e-3)])
            elif float(l)<= 0.80:
                p0 = np.array([2,20,15*(s*1e-3)])
            elif float(l)<= 0.83:
                p0 = np.array([2,18,15*(s*1e-3)])
            elif float(l)<= 0.85:
                p0 = np.array([2,12,30*(s*1e-3)])
            elif float(l)<= 0.875:
                p0 = np.array([1.5,8,40*(s*1e-3)])
            elif float(l)==0.885 and r=="2":
                p0 = np.array([0.5,2,40*(s*1e-3)])
            elif float(l)<= 0.895:
                p0 = np.array([0.5,4.5,80*(s*1e-3)])
            elif float(l)<= 0.95:
                p0 = np.array([0.2,0.8,130*(s*1e-3)])
            else:
                print("Please adjust lines 50-70 to the new legitimacy values")
                return
    
        elif pm == "1":
            if float(l)<= 0.40:
                p0 = np.array([2,25,15*(s*1e-3)])
            elif float(l)<= 0.60:
                p0 = np.array([2,20,15*(s*1e-3)])
            elif float(l)<= 0.70:
                p0 = np.array([2,18,15*(s*1e-3)])
            elif float(l)<= 0.71:
                p0 = np.array([2,24,15*(s*1e-3)])
            elif float(l)<= 0.75:
                p0 = np.array([2,21,15*(s*1e-3)])
            elif float(l)<= 0.80:
                p0 = np.array([2,20,15*(s*1e-3)])
            elif float(l)<= 0.83:
                p0 = np.array([2,18,15*(s*1e-3)])
            elif float(l)<= 0.85:
                p0 = np.array([2,17,30*(s*1e-3)])
            elif float(l)<= 0.875:
                p0 = np.array([1.5,10,40*(s*1e-3)])
            elif float(l)<= 0.885:
                p0 = np.array([1,5,60*(s*1e-3)])
            elif float(l)==0.895 or float(l)==0.8975 and r=="2":
                p0 = np.array([0.5,1,80*(s*1e-3)])
            elif float(l)<= 1:
                p0 = np.array([0.5,2,80*(s*1e-3)])
            else:
                print("Please adjust lines 70-90 to the new legitimacy values")
                return
        
        elif pm == "2" and f == "0":
            if float(l)==0.43 and r=="1":
                p0 = np.array([2,37,27*(s*1e-3)])
            elif float(l)<= 0.45:
                p0 = np.array([2,42,27*(s*1e-3)])
            elif float(l)<= 0.55:
                p0 = np.array([2,39,27*(s*1e-3)])
            elif float(l)==0.5650 and r=="4":
                p0 = np.array([2,40,27*(s*1e-3)])
            elif float(l)<= 0.60:
                p0 = np.array([2,35,27*(s*1e-3)])
            elif float(l)<= 0.65:
                p0 = np.array([2,35,25*(s*1e-3)])
            elif float(l)<= 0.70:
                p0 = np.array([2,32,25*(s*1e-3)])
            elif float(l)<= 0.75:
                p0 = np.array([2,30,20*(s*1e-3)])
            elif float(l)== 0.7675 and r == "1":
                p0 = np.array([2,26,20*(s*1e-3)])
            elif float(l)<= 0.780:
                p0 = np.array([2,23,20*(s*1e-3)])
            elif float(l)<= 0.80:
                p0 = np.array([2,20,20*(s*1e-3)])
            elif float(l)<= 0.83:
                p0 = np.array([2,18,15*(s*1e-3)])
            elif float(l)<= 0.85:
                p0 = np.array([2,12,30*(s*1e-3)])
            elif float(l)<= 0.87:
                p0 = np.array([2,10,40*(s*1e-3)])
            elif float(l)<= 0.885:
                p0 = np.array([1.5,5,40*(s*1e-3)])
            elif float(l)<= 0.89:
                p0 = np.array([1.5,4,40*(s*1e-3)])
            elif float(l)<= 0.95:
                p0 = np.array([0.5,1.5,90*(s*1e-3)])
            else:
                print("Please adjust lines 100-120 to the new legitimacy values")  
                return
            
        elif pm == "2" and f == "1":
            if float(l)==0.54 and r=="3":
                p0 = np.array([2,40,35*(s*1e-3)])
            elif float(l)<= 0.54:
                p0 = np.array([2,45,35*(s*1e-3)])
            elif (float(l)==0.55 or float(l)==0.56) and r=="3":
                p0 = np.array([2,43,35*(s*1e-3)])
            elif float(l)==0.555 and r=="4":
                p0 = np.array([2,43,35*(s*1e-3)])
            elif (float(l)==0.565) and r=="2":
                p0 = np.array([2,43,35*(s*1e-3)])
            elif float(l)<= 0.65:
                p0 = np.array([2,40,35*(s*1e-3)])
            elif float(l)<= 0.70:
                p0 = np.array([2,40,25*(s*1e-3)])
            elif float(l)<= 0.75:
                p0 = np.array([2,35,25*(s*1e-3)])
            elif float(l)<= 0.775:
                p0 = np.array([2,33,25*(s*1e-3)])
            elif float(l)<= 0.80:
                p0 = np.array([2,30,25*(s*1e-3)])
            elif (float(l)==0.8075) and r=="3":
                p0 = np.array([2,32,35*(s*1e-3)])
            elif float(l)<= 0.83:
                p0 = np.array([2,26,28*(s*1e-3)])
            elif float(l)<= 0.85:
                p0 = np.array([2,22,35*(s*1e-3)])
            elif float(l)<= 0.87:
                p0 = np.array([2,16,45*(s*1e-3)])
            elif float(l)<= 0.8775:
                p0 = np.array([2,10,45*(s*1e-3)])
            elif float(l)<= 0.885:
                p0 = np.array([1.5,9,50*(s*1e-3)])
            elif float(l)==0.8875 and (r=="1" or r=="2"):
                p0 = np.array([0.5,7,100*(s*1e-3)])
            elif float(l)<= 0.89:
                p0 = np.array([0.5,5.5,100*(s*1e-3)])
            elif float(l)==0.8925 and r=="1":
                p0 = np.array([0.5,4,100*(s*1e-3)])
            elif float(l)<= 0.895:
                p0 = np.array([0.5,3,100*(s*1e-3)])
            elif float(l)<= 0.95:
                p0 = np.array([0.5,1,100*(s*1e-3)])
            else:
                print("Please adjust lines 120-150 to the new legitimacy values")
                return
    
        elif pm == "3" and f == "0":
            if float(l)==0.35 and r=="3":
                p0 = np.array([2,25,15*(s*1e-3)])
            elif float(l)<= 0.40:
                p0 = np.array([2,28,15*(s*1e-3)])
            elif float(l)<= 0.60:
                p0 = np.array([2,26,15*(s*1e-3)])
            elif float(l)<= 0.65:
                p0 = np.array([2,23,15*(s*1e-3)])
            elif float(l)<= 0.70:
                p0 = np.array([2,20,20*(s*1e-3)])
            elif float(l)<= 0.75:
                p0 = np.array([2,20,20*(s*1e-3)])
            elif float(l)<= 0.80:
                p0 = np.array([2,17,20*(s*1e-3)])
            elif float(l)<= 0.83:
                p0 = np.array([2,16,20*(s*1e-3)])
            elif float(l)<= 0.85:
                p0 = np.array([2,12,30*(s*1e-3)])
            elif float(l)<= 0.875:
                p0 = np.array([1.5,8,35*(s*1e-3)])
            elif float(l)<= 0.885:
                p0 = np.array([1,5,60*(s*1e-3)])
            elif float(l)==0.8875 and r =="2":
                p0 = np.array([1,3.5,60*(s*1e-3)])
            elif float(l)<= 0.95:
                p0 = np.array([0.5,2,60*(s*1e-3)])
            else:
                print("Please adjust lines 150-170 to the new legitimacy values")
                return

        elif pm == "3" and f == "1":
            if float(l)<= 0.40:
                p0 = np.array([3,32,15*(s*1e-3)])
            elif float(l)<= 0.60:
                p0 = np.array([2,30,15*(s*1e-3)])
            elif float(l)<= 0.65:
                p0 = np.array([2,28,15*(s*1e-3)])
            elif float(l)<= 0.70:
                p0 = np.array([2,27,15*(s*1e-3)])
            elif float(l)<= 0.75:
                p0 = np.array([2,27,15*(s*1e-3)])
            elif float(l)<= 0.77:
                p0 = np.array([2,24,15*(s*1e-3)])
            elif float(l)<= 0.80:
                p0 = np.array([2,22,15*(s*1e-3)])
            elif float(l)<= 0.83:
                p0 = np.array([2,18,15*(s*1e-3)])
            elif float(l)<= 0.85:
                p0 = np.array([2,17,30*(s*1e-3)])
            elif float(l)<= 0.875:
                p0 = np.array([1.5,12,40*(s*1e-3)])
            elif float(l)<= 0.885:
                p0 = np.array([1.5,8,45*(s*1e-3)])
            elif float(l)==0.8875 and (r=="3" or r=="4"):
                p0 = np.array([1,5,45*(s*1e-3)])
            elif float(l)<= 0.95:
                p0 = np.array([0.5,3,45*(s*1e-3)])
            else:
                print("Please adjust lines 170-190 to the new legitimacy values")
                return
                
        elif pm=="4":
            if float(l)==0.05 and r=="4":
                p0 = np.array([2,27,15*(s*1e-3)])  
            elif float(l)<= 0.45:
                p0 = np.array([2,35,15*(s*1e-3)])  
            elif float(l)<= 0.60:
                p0 = np.array([2,30,15*(s*1e-3)])
            elif float(l)<= 0.65:
                p0 = np.array([2,28,15*(s*1e-3)])
            elif float(l)<= 0.70:
                p0 = np.array([2,25,15*(s*1e-3)])
            elif float(l)<= 0.75:
                p0 = np.array([2,21,15*(s*1e-3)])
            elif float(l)<= 0.80:
                p0 = np.array([2,20,15*(s*1e-3)])
            elif float(l)<= 0.83:
                p0 = np.array([2,18,15*(s*1e-3)])
            elif float(l)<= 0.85:
                p0 = np.array([2,17,30*(s*1e-3)])
            elif float(l)<= 0.875:
                p0 = np.array([1.5,10,40*(s*1e-3)])
            elif float(l)<= 0.885:
                p0 = np.array([1.5,8,45*(s*1e-3)])
            elif float(l)==0.8875 and r=="4":
                p0 = np.array([1,4,45*(s*1e-3)])
            elif float(l)<= 0.8925:
                p0 = np.array([0.5,2,70*(s*1e-3)])
            elif float(l)<=1:
                p0 = np.array([0.5,0.5,70*(s*1e-3)])
            else:
                print("Please adjust lines 200-220 to the new legitimacy values")
                return
                
        elif pm == "5":
            if float(l)<=0.20:
                p0 = np.array([2,44,15*(s*1e-3)])
            elif float(l)<= 0.60:
                p0 = np.array([2,38,15*(s*1e-3)])
            elif float(l)<= 0.65:
                p0 = np.array([2,35,15*(s*1e-3)])
            elif float(l)<= 0.70:
                p0 = np.array([2,30,15*(s*1e-3)])
            elif float(l)<= 0.75:
                p0 = np.array([2,28,15*(s*1e-3)])
            elif float(l)<= 0.80:
                p0 = np.array([2,22,15*(s*1e-3)])
            elif float(l)<= 0.83:
                p0 = np.array([2,18,15*(s*1e-3)])
            elif float(l)<= 0.85:
                p0 = np.array([2,17,30*(s*1e-3)])
            elif float(l)<= 0.875:
                p0 = np.array([1.5,13,40*(s*1e-3)])
            elif float(l)<= 0.885:
                p0 = np.array([1.5,6,45*(s*1e-3)])
            elif float(l)<= 0.90:
                p0 = np.array([1.0,3,45*(s*1e-3)])
            elif float(l)<= 0.95:
                p0 = np.array([0.5,2,100*(s*1e-3)])
            else:
                print("Please adjust lines 220-240 to the new legitimacy values")
                return
        
        else:
            print("pm or f input not valid (0 <= pm <= 5, f = 1 or f = 0)")
            return
        
    elif t == "j":
        
        if pm == "0":
            if float(js)<= 5:
                p0 = np.array([1.5,59,25*(s*1e-3)])
            elif float(js)==6 and r=="3":
                p0 = np.array([2,60,25*(s*1e-3)])
            elif float(js)<= 15:
                p0 = np.array([2,50,25*(s*1e-3)])
            elif float(js)<= 20:
                p0 = np.array([2,47,20*(s*1e-3)])
            elif float(js)<= 25:
                p0 = np.array([2,40,15*(s*1e-3)])
            elif float(js)<= 30:
                p0 = np.array([2,45,10*(s*1e-3)])
            elif float(js)<= 40:
                p0 = np.array([2,38,10*(s*1e-3)])
            elif float(js)<= 50:
                p0 = np.array([2,30,10*(s*1e-3)])
            elif float(js)<= 55:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 60:
                p0 = np.array([2,27,10*(s*1e-3)])
            elif float(js)<= 65:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 70:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 77:
                p0 = np.array([2,22,10*(s*1e-3)])
            elif float(js)<= 80:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 85:
                p0 = np.array([1.5,23,10*(s*1e-3)])
            elif float(js)<= 87:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 92:
                p0 = np.array([1.5,22,10*(s*1e-3)])
            elif float(js)<= 93:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)==94 and r=="3":
                p0 = np.array([1.5,11,10*(s*1e-3)])
            elif float(js)<= 94:
                p0 = np.array([1.5,25,10*(s*1e-3)])
            elif float(js)<= 104:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 109:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 113:
                p0 = np.array([1.5,17.5,10*(s*1e-3)])
            elif float(js)<= 119:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 120:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 125:
                p0 = np.array([1.5,10,10*(s*1e-3)])
            elif float(js)<= 127:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 128:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 129:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 130:
                p0 = np.array([1.5,16,10*(s*1e-3)])
            elif float(js)<= 133:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 134:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 136:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 137:
                p0 = np.array([1.5,10,10*(s*1e-3)])
            elif float(js)<= 143:
                p0 = np.array([1.5,9,10*(s*1e-3)])
            elif float(js)<= 148:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 149:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 180:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 200:
                p0 = np.array([1.5,10,10*(s*1e-3)])
        elif pm == "1":
            if float(js)<= 2:
                p0 = np.array([1.5,53,10*(s*1e-3)])
            elif float(js)==4 and r=="2":
                p0 = np.array([1.5,52,10*(s*1e-3)])
            elif float(js)<= 8:
                p0 = np.array([1.5,45,10*(s*1e-3)])
            elif float(js)<= 12:
                p0 = np.array([1.5,40,10*(s*1e-3)])
            elif float(js)<= 16:
                p0 = np.array([1.5,35,10*(s*1e-3)])
            elif float(js)<= 22:
                p0 = np.array([1.5,30,10*(s*1e-3)])
            elif float(js)<= 28:
                p0 = np.array([1.5,25,10*(s*1e-3)])
            elif float(js)== 30 and r=="1":
                p0 = np.array([1.5,24,10*(s*1e-3)])
            elif float(js)<= 40:
                p0 = np.array([1.5,20,10*(s*1e-3)])
            elif float(js)<= 50:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 70:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 82:
                p0 = np.array([1.5,10,10*(s*1e-3)])
            elif float(js)<= 138:
                p0 = np.array([1.5,8,10*(s*1e-3)])
            elif float(js)<= 200:
                p0 = np.array([1.5,5,10*(s*1e-3)])
                
        elif pm == "2" and f=="0":
            if float(js)==2 and r=="4":
                p0 = np.array([1.5,57,25*(s*1e-3)])
            elif float(js)<= 5:
                p0 = np.array([1.5,59,25*(s*1e-3)])
            elif float(js)==6 and r=="3":
                p0 = np.array([2,60,25*(s*1e-3)])
            elif float(js)==12 and r=="2":
                p0 = np.array([2,47,25*(s*1e-3)])
            elif float(js)<= 15:
                p0 = np.array([2,50,25*(s*1e-3)])
            elif float(js)<= 20:
                p0 = np.array([2,47,20*(s*1e-3)])
            elif float(js)<= 25:
                p0 = np.array([2,40,15*(s*1e-3)])
            elif float(js)<= 30:
                p0 = np.array([2,45,10*(s*1e-3)])
            elif float(js)<= 40:
                p0 = np.array([2,38,10*(s*1e-3)])
            elif float(js)<= 50:
                p0 = np.array([2,30,10*(s*1e-3)])
            elif float(js)<= 55:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 60:
                p0 = np.array([2,27,10*(s*1e-3)])
            elif float(js)<= 65:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 70:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 77:
                p0 = np.array([2,22,10*(s*1e-3)])
            elif float(js)== 80 and r=="3":
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 80:
                p0 = np.array([1.5,20,10*(s*1e-3)])
            elif float(js)<= 85:
                p0 = np.array([1.5,23,10*(s*1e-3)])
            elif float(js)<= 87:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 92:
                p0 = np.array([1.5,22,10*(s*1e-3)])
            elif float(js)<= 93:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)== 94 and r=="4":
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 94:
                p0 = np.array([1.5,25,10*(s*1e-3)])
            elif float(js)<= 104:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 109:
                p0 = np.array([1.5,16,10*(s*1e-3)])
            elif float(js)<= 113:
                p0 = np.array([1.5,17.5,10*(s*1e-3)])
            elif float(js)<= 119:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 120:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 125:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 127:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 128:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 129:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 130:
                p0 = np.array([1.5,16,10*(s*1e-3)])
            elif float(js)<= 133:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 134:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 136:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 137:
                p0 = np.array([1.5,10,10*(s*1e-3)])
            elif float(js)<= 143:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 148:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 149:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 180:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 200:
                p0 = np.array([1.5,10,10*(s*1e-3)])
                
        elif pm == "2" and f=="1":
            if float(js)==0:
                p0 = np.array([1.5,60,25*(s*1e-3)])
            elif float(js)<= 8:
                p0 = np.array([1.5,58,25*(s*1e-3)])
            elif float(js)<= 12:
                p0 = np.array([2,55,25*(s*1e-3)])
            elif float(js)<= 20:
                p0 = np.array([2,50,20*(s*1e-3)])
            elif float(js)<= 30:
                p0 = np.array([2,44,10*(s*1e-3)])
            elif float(js)<= 40:
                p0 = np.array([2,39,10*(s*1e-3)])
            elif float(js)<= 50:
                p0 = np.array([2,39,10*(s*1e-3)])
            elif float(js)<= 55:
                p0 = np.array([2,37,10*(s*1e-3)])
            elif float(js)<= 60:
                p0 = np.array([2,35,10*(s*1e-3)])
            elif float(js)<= 65:
                p0 = np.array([2,32,10*(s*1e-3)])
            elif float(js)<= 80:
                p0 = np.array([1.5,31,10*(s*1e-3)])
            elif float(js)<= 85:
                p0 = np.array([1.5,27,10*(s*1e-3)])
            elif float(js)<= 109:
                p0 = np.array([1.5,25,10*(s*1e-3)])
            elif float(js)<= 113:
                p0 = np.array([1.5,23,10*(s*1e-3)])
            elif float(js)<= 119:
                p0 = np.array([1.5,22,10*(s*1e-3)])
            elif float(js)<= 125:
                p0 = np.array([1.5,21,10*(s*1e-3)])
            elif float(js)<= 127:
                p0 = np.array([1.5,22,10*(s*1e-3)])
            elif float(js)<= 128:
                p0 = np.array([1.5,20,10*(s*1e-3)])
            elif float(js)<= 130:
                p0 = np.array([1.5,24,10*(s*1e-3)])
            elif float(js)<= 133:
                p0 = np.array([1.5,22,10*(s*1e-3)])
            elif float(js)<= 149:
                p0 = np.array([1.5,20,10*(s*1e-3)])
            elif float(js)<= 180:
                p0 = np.array([1.5,19,10*(s*1e-3)])
            elif float(js)<= 200:
                p0 = np.array([1.5,18,10*(s*1e-3)])
                
        elif pm == "3" and f=="0":
            if float(js)== 0 and (r=="3" or r=="2"):
                p0 = np.array([1.5,57,25*(s*1e-3)])
            elif float(js)<= 5:
                p0 = np.array([1.5,59,25*(s*1e-3)])
            elif float(js)==6 and r=="3":
                p0 = np.array([2,60,25*(s*1e-3)])
            elif float(js)<= 15:
                p0 = np.array([2,50,25*(s*1e-3)])
            elif float(js)<= 20:
                p0 = np.array([2,47,20*(s*1e-3)])
            elif float(js)<= 25:
                p0 = np.array([2,40,15*(s*1e-3)])
            elif float(js)<= 30:
                p0 = np.array([2,45,10*(s*1e-3)])
            elif float(js)<= 40:
                p0 = np.array([2,38,10*(s*1e-3)])
            elif float(js)<= 50:
                p0 = np.array([2,30,10*(s*1e-3)])
            elif float(js)<= 55:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 60:
                p0 = np.array([2,27,10*(s*1e-3)])
            elif float(js)<= 65:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 70:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 77:
                p0 = np.array([2,22,10*(s*1e-3)])
            elif float(js)<= 80:
                p0 = np.array([1.5,20,10*(s*1e-3)])
            elif float(js)<= 85:
                p0 = np.array([1.5,23,10*(s*1e-3)])
            elif float(js)<= 87:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 92:
                p0 = np.array([1.5,22,10*(s*1e-3)])
            elif float(js)<= 93:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 94:
                p0 = np.array([1.5,11,10*(s*1e-3)])
            elif float(js)<= 104:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 109:
                p0 = np.array([1.5,16,10*(s*1e-3)])
            elif float(js)<= 113:
                p0 = np.array([1.5,17.5,10*(s*1e-3)])
            elif float(js)<= 119:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 120:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 125:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 127:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 128:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 129:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 130:
                p0 = np.array([1.5,16,10*(s*1e-3)])
            elif float(js)<= 133:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 134:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 136:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 137:
                p0 = np.array([1.5,10,10*(s*1e-3)])
            elif float(js)<= 143:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 148:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 149:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 180:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 200:
                p0 = np.array([1.5,10,10*(s*1e-3)])

        elif pm == "3" and f=="1":
            if float(js)<= 5:
                p0 = np.array([1.5,59,25*(s*1e-3)])
            elif float(js)==6 and r=="3":
                p0 = np.array([2,60,25*(s*1e-3)])
            elif float(js)==6 and r=="2":
                p0 = np.array([2,54,25*(s*1e-3)])
            elif float(js)<= 15:
                p0 = np.array([2,50,25*(s*1e-3)])
            elif float(js)<= 20:
                p0 = np.array([2,47,20*(s*1e-3)])
            elif float(js)<= 25:
                p0 = np.array([2,40,15*(s*1e-3)])
            elif float(js)<= 30:
                p0 = np.array([2,45,10*(s*1e-3)])
            elif float(js)<= 40:
                p0 = np.array([2,38,10*(s*1e-3)])
            elif float(js)<= 50:
                p0 = np.array([2,30,10*(s*1e-3)])
            elif float(js)<= 55:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 60:
                p0 = np.array([2,27,10*(s*1e-3)])
            elif float(js)<= 65:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 70:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 77:
                p0 = np.array([2,22,10*(s*1e-3)])
            elif float(js)<= 80:
                p0 = np.array([1.5,20,10*(s*1e-3)])
            elif float(js)<= 85:
                p0 = np.array([1.5,23,10*(s*1e-3)])
            elif float(js)<= 87:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 92:
                p0 = np.array([1.5,22,10*(s*1e-3)])
            elif float(js)<= 93:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 94:
                p0 = np.array([1.5,25,10*(s*1e-3)])
            elif float(js)<= 104:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 109:
                p0 = np.array([1.5,16,10*(s*1e-3)])
            elif float(js)<= 113:
                p0 = np.array([1.5,17.5,10*(s*1e-3)])
            elif float(js)<= 119:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 120:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 125:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 127:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 128:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 129:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 130:
                p0 = np.array([1.5,16,10*(s*1e-3)])
            elif float(js)<= 133:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 134:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 136:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 137:
                p0 = np.array([1.5,10,10*(s*1e-3)])
            elif float(js)<= 143:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 148:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 149:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 180:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 200:
                p0 = np.array([1.5,10,10*(s*1e-3)])
        
        elif pm == "4":
            if float(js)==0 and r=="3":
                p0 = np.array([1.5,62.5,25*(s*1e-3)])
            elif float(js)<= 5:
                p0 = np.array([1.5,59,25*(s*1e-3)])
            elif float(js)==6 and r=="3":
                p0 = np.array([2,60,25*(s*1e-3)])
            elif float(js)<= 15:
                p0 = np.array([2,50,25*(s*1e-3)])
            elif float(js)<= 20:
                p0 = np.array([2,47,20*(s*1e-3)])
            elif float(js)<= 25:
                p0 = np.array([2,40,15*(s*1e-3)])
            elif float(js)<= 30:
                p0 = np.array([2,24,10*(s*1e-3)])
            elif float(js)<= 40:
                p0 = np.array([2,20,10*(s*1e-3)])
            elif float(js)<= 50:
                p0 = np.array([2,30,10*(s*1e-3)])
            elif float(js)<= 55:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 60:
                p0 = np.array([2,27,10*(s*1e-3)])
            elif float(js)<= 65:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 70:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 77:
                p0 = np.array([2,22,10*(s*1e-3)])
            elif float(js)<= 80:
                p0 = np.array([1.5,20,10*(s*1e-3)])
            elif float(js)== 84 and r=="4":
                p0 = np.array([1.5,10,10*(s*1e-3)])
            elif float(js)<= 85:
                p0 = np.array([1.5,23,10*(s*1e-3)])
            elif float(js)<= 87:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 92:
                p0 = np.array([1.5,10,10*(s*1e-3)])
            elif float(js)<= 93:
                p0 = np.array([1.5,10,10*(s*1e-3)])
            elif float(js)<= 94:
                p0 = np.array([1.5,9,10*(s*1e-3)])
            elif float(js)<= 104:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 109:
                p0 = np.array([1.5,16,10*(s*1e-3)])
            elif float(js)<= 113:
                p0 = np.array([1.5,17.5,10*(s*1e-3)])
            elif float(js)<= 119:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 120:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 125:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 127:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 128:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 129:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 130:
                p0 = np.array([1.5,16,10*(s*1e-3)])
            elif float(js)<= 133:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 134:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 136:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 137:
                p0 = np.array([1.5,10,10*(s*1e-3)])
            elif float(js)<= 143:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 148:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 149:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 180:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 200:
                p0 = np.array([1.5,10,10*(s*1e-3)])
        
        elif pm == "5" and f=="0":
            if float(js)<= 5:
                p0 = np.array([1.5,59,25*(s*1e-3)])
            elif float(js)==6 and r=="3":
                p0 = np.array([2,60,25*(s*1e-3)])
            elif float(js)==6 and r=="2":
                p0 = np.array([2,55,25*(s*1e-3)])
            elif float(js)<= 15:
                p0 = np.array([2,50,25*(s*1e-3)])
            elif float(js)==20 and r=="3":
                p0 = np.array([2,43,25*(s*1e-3)])
            elif float(js)<= 20:
                p0 = np.array([2,47,20*(s*1e-3)])
            elif float(js)<= 25:
                p0 = np.array([2,40,15*(s*1e-3)])
            elif float(js)<= 30:
                p0 = np.array([2,45,10*(s*1e-3)])
            elif float(js)<= 40:
                p0 = np.array([2,38,10*(s*1e-3)])
            elif float(js)<= 50:
                p0 = np.array([2,30,10*(s*1e-3)])
            elif float(js)<= 55:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 60:
                p0 = np.array([2,27,10*(s*1e-3)])
            elif float(js)<= 65:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 70:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 77:
                p0 = np.array([2,22,10*(s*1e-3)])
            elif float(js)<= 80:
                p0 = np.array([1.5,20,10*(s*1e-3)])
            elif float(js)<= 85:
                p0 = np.array([1.5,23,10*(s*1e-3)])
            elif float(js)<= 87:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 92:
                p0 = np.array([1.5,22,10*(s*1e-3)])
            elif float(js)<= 93:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 94:
                p0 = np.array([1.5,10,10*(s*1e-3)])
            elif float(js)<= 104:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 109:
                p0 = np.array([1.5,16,10*(s*1e-3)])
            elif float(js)<= 113:
                p0 = np.array([1.5,17.5,10*(s*1e-3)])
            elif float(js)<= 119:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 120:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 125:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 127:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 128:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 129:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 130:
                p0 = np.array([1.5,16,10*(s*1e-3)])
            elif float(js)<= 133:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 134:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 136:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 137:
                p0 = np.array([1.5,10,10*(s*1e-3)])
            elif float(js)<= 143:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 148:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 149:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<=200 and r=="1":
                p0 = np.array([1.5,5,10*(s*1e-3)])
            elif float(js)<= 180:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 200:
                p0 = np.array([1.5,10,10*(s*1e-3)])
            
        elif pm == "5" and f=="1":
            if float(js)<= 5:
                p0 = np.array([1.5,59,25*(s*1e-3)])
            elif float(js)==6 and r=="3":
                p0 = np.array([2,60,25*(s*1e-3)])
            elif float(js)==6 and r=="1":
                p0 = np.array([2,47.5,25*(s*1e-3)])
            elif float(js)<= 15:
                p0 = np.array([2,50,25*(s*1e-3)])
            elif float(js)<= 20:
                p0 = np.array([2,47,20*(s*1e-3)])
            elif float(js)<= 25:
                p0 = np.array([2,40,15*(s*1e-3)])
            elif float(js)== 30 and r=="2":
                p0 = np.array([2,26,10*(s*1e-3)])
            elif float(js)<= 30:
                p0 = np.array([2,29,10*(s*1e-3)])
            elif float(js)<= 40:
                p0 = np.array([2,38,10*(s*1e-3)])
            elif float(js)<= 50:
                p0 = np.array([2,30,10*(s*1e-3)])
            elif float(js)<= 55:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 60:
                p0 = np.array([2,27,10*(s*1e-3)])
            elif float(js)<= 65:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 70:
                p0 = np.array([2,28,10*(s*1e-3)])
            elif float(js)<= 77:
                p0 = np.array([2,22,10*(s*1e-3)])
            elif float(js)<= 80:
                p0 = np.array([1.5,20,10*(s*1e-3)])
            elif float(js)<= 85:
                p0 = np.array([1.5,23,10*(s*1e-3)])
            elif float(js)<= 87:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 92:
                p0 = np.array([1.5,22,10*(s*1e-3)])
            elif float(js)<= 93:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 94:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 104:
                p0 = np.array([1.5,18,10*(s*1e-3)])
            elif float(js)<= 109:
                p0 = np.array([1.5,16,10*(s*1e-3)])
            elif float(js)<= 113:
                p0 = np.array([1.5,17.5,10*(s*1e-3)])
            elif float(js)<= 119:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 120:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 125:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<= 127:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 128:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 129:
                p0 = np.array([1.5,13,10*(s*1e-3)])
            elif float(js)<= 130:
                p0 = np.array([1.5,16,10*(s*1e-3)])
            elif float(js)<= 133:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 134:
                p0 = np.array([1.5,17,10*(s*1e-3)])
            elif float(js)<= 136:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 137:
                p0 = np.array([1.5,10,10*(s*1e-3)])
            elif float(js)<= 143:
                p0 = np.array([1.5,14,10*(s*1e-3)])
            elif float(js)<= 148:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 149:
                p0 = np.array([1.5,15,10*(s*1e-3)])
            elif float(js)<=200 and r=="1":
                p0 = np.array([1.5,5,10*(s*1e-3)])
            elif float(js)<= 180:
                p0 = np.array([1.5,12,10*(s*1e-3)])
            elif float(js)<= 200:
                p0 = np.array([1.5,10,10*(s*1e-3)])
                
                
    Param,CovMat = curve_fit(fit,x_fit1,freq,p0)
    x_fit2 = np.arange(0,100,100/1120) #longer array for plotting
    y_fit = fit(x_fit2,Param[0],Param[1],Param[2])
    
    #Histogram plot
    plt.hist(data, bins=1120, width = 100/1120)
    if with_fit == True:
        plt.plot(x_fit2,y_fit,'-k')
    plt.xlabel("Percentage of Active Agents")
    full_l = "%.4f" % (float(l)) #so that l=0.5 is saved as l=0.5000
    if t=="j":
        full_js = "%.2f" % (float(js))
    else:
        full_js = js
    filename_hist_jpeg = "hist_pm" + pm + "_f" + f + "_l" + full_l + "_cm" + cm + "_js" + full_js + "_run"+r+".jpeg"
    full_path_hist_jpeg = os.path.join(direc_out_hist, filename_hist_jpeg)
    plt.savefig(full_path_hist_jpeg)        
    if show == True:
        print("pm" + pm + " f" + f + " l" + l + " cm" + cm +
              " js" + js)
        print(p0)
        plt.show()
        
    return abs(Param[0]),Param[1]



l_it =0
js_it = 0
for l in legitimacies:
    for js in max_jail_sentences:
        for cm in citizen_movements:
            if pm != "1":
                if l[-1] == "0":
                    l = l[:-1]
                    if l[-1] == "0":
                        l = l[:-1]
                        if l[-1] == "0":
                            l = l[:-1]
            if t == "j" and (pm=="1" or (pm == "5" and (f!=0 or r!="4"))):
                l = "0.70"
            if r=="1":
                filename_txt = "pm" + pm + "_f" + f + "_l" + l + "_cm" + cm + "_js" + js + ".txt"
            else:
                filename_txt = "pm" + pm + "_f" + f + "_l" + l + "_cm" + cm + "_js" + js + "_run"+r+".txt"
            full_path_txt = os.path.join(direc_in, filename_txt)
            data_pandas = pd.read_csv(full_path_txt, sep=' ', header=None, skiprows=1)
            data = data_pandas.to_numpy()
            if t == "l":
                print("Fitting to legitimacy " +l)
            elif t == "j":
                print("Fitting to jail sentence " +js)

            sigma,mu = hist(data[20:, 0], pm, f, l, cm, js,t, show=False, with_fit = True) #20 steps for equilibrium to set in
            #hist(data[20:, 0], pm, f, l, cm, js,t, show=True, with_fit = True) #20 steps for equilibrium to set in
            sigma_arr[l_it,js_it] = sigma 
            mu_arr[l_it,js_it] = mu
        js_it +=1
    l_it +=1
    js_it = 0



if not os.path.exists("extracted_data/pickled_data"):
    os.makedirs("extracted_data/pickled_data")
if t == "j":
    pickle.dump(mu_arr, open( "extracted_data/pickled_data/mean_pm"+pm+"_f"+f+"_run"+r+"_js.p", "wb" ))
    pickle.dump(sigma_arr, open( "extracted_data/pickled_data/std_pm"+pm+"_f"+f+"_run"+r+"_js.p", "wb" ))
else:
    pickle.dump(mu_arr, open( "extracted_data/pickled_data/mean_pm"+pm+"_f"+f+"_run"+r+".p", "wb" ))
    pickle.dump(sigma_arr, open( "extracted_data/pickled_data/std_pm"+pm+"_f"+f+"_run"+r+".p", "wb" ))




    
    
    
