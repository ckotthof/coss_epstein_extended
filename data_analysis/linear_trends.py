# -*- coding: utf-8 -*-
"""
Created on Sun Nov 15 15:24:24 2020

@author: crkau
"""

import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import sys
import os
from uncertainties import unumpy, ufloat_fromstr
from uncertainties.umath import *

if (len(sys.argv) != 9 and len(sys.argv) != 8 and len(sys.argv) != 7 and len(sys.argv) != 6):
    print(
        "\nCall this program by 'python linear_trends.py  <i_all> <police_movement_type> <forced_line> <jail sentence/legitimacy> <macro/micro> <fit?> <runs> <var>'\ni_all is True of False plot all combos, then pm and f are arbitrary\nIf i_all = True, no pm and f needed (ignored if given)\npolice_movement_value is an int\nforced_line is 1 or 0\njs/leg is a float: which jail sentence length or legitimacy the varied legitimacies/jail sentences have as constant\nOnly plots actives\nmacro/micro is bool: whether to plot macro and micro movement types in separate graphs\nfit? is True or False: whether to fit data (optional)\n runs is int number of runs\nvar is 'js' or 'leg' which variable is fitted or plotted against (max jail sentences or legitimacies varied)")
    exit()
elif (len(sys.argv) == 9):
    i_all = sys.argv[1]
    pm = sys.argv[2]
    f = sys.argv[3]
    js = float(sys.argv[4])
    i_mm = sys.argv[5]
    i_fit = sys.argv[6]
    run = int(sys.argv[7])
    fitting = sys.argv[8]
elif (len(sys.argv) == 8):
    i_all = sys.argv[1]
    pm = sys.argv[2]
    f = sys.argv[3]
    js = float(sys.argv[4])
    i_mm = sys.argv[5]
    run = int(sys.argv[6])
    fitting = sys.argv[7]
elif (len(sys.argv) == 7):
    i_all = sys.argv[1]
    js = float(sys.argv[2])
    i_mm = sys.argv[3]
    i_fit = sys.argv[4]
    run = int(sys.argv[5])
    fitting = sys.argv[6]
elif (len(sys.argv) == 6):
    i_all = sys.argv[1]
    js = float(sys.argv[2])
    i_mm = sys.argv[3]
    run = int(sys.argv[4])
    fitting = sys.argv[5]

if fitting == 'js':
    l = js


if not (fitting == 'js' or fitting == 'leg' and run >= 0):
    print(
        "\nInvalid Call\n")
    exit()
    
    
if not os.path.exists('graphs_linear_js'):
    os.makedirs('graphs_linear_js')
if not os.path.exists('graphs_linear_leg'):
    os.makedirs('graphs_linear_leg')

#Legitimacies used for our final data
legitimacies_arr1 = np.arange(0,40,5)
legitimacies_arr2 = np.arange(40,55,1)
legitimacies_arr3 = np.arange(55,90,0.25)
legitimacies = np.concatenate((legitimacies_arr1,legitimacies_arr2,legitimacies_arr3))
#Max jail sentence lengths used for our final data
max_jail_sentences = np.arange(1, 100, 1)*2
citizen_movements = ["0"] #arbitrary here, just for file naming reference
if fitting == 'js':
    direc_in = r'./linear_js/'
    direc_out = r'./graphs_linear_js/'
if fitting == 'leg':
    direc_in = r'./linear_leg/'
    direc_out = r'./graphs_linear_leg/'

if fitting == 'js':
    legitimacies = [l*100]
elif fitting == 'leg':
    max_jail_sentences = [js]

#Colors of different pm movement types
color00 = "#f5d838" #yellow
color10 = "#ff5f16" #orange
color20 = "#8B5DF3" #light violet
color21 = "#22055F" #dark violet
color30 = "#2ed8ba" #light turquoise
color31 = "#093E6C" #dark turquoise
color40 = "#ff0f0f" #red
color50 = "#bae672" #light green
color51 = "#067a00" #dark green

colors = [color00, color10, color20, color21, color30, color31, color40, color50, color51]
indices_color = [0, 1, 2, 4, 6, 7]

if i_fit == 'True':
    if fitting == 'leg':
        filepath_fit = os.path.join(direc_out, "offset_vs_leg_fitparam.txt")
    else:
        filepath_fit = os.path.join(direc_out, "offset_vs_js_fitparam.txt")
    if not os.path.exists(filepath_fit):
        file_a = open(filepath_fit, "w+")
        if fitting == 'leg':
            file_a.write('Actives Offset vs Legitimacy Fit y = a*(b-x)**rho + c \n \n')
        else:
            file_a.write('Actives Offset vs Maximum Jail Sentence Fit y = a*exp(b*(x-c)) + d \n \n')
        file_a.close()

def read(pm, f, run=1):
    if run == 1:
        filename = 'linfitparams_actives' + str(pm) + str(f) + '.txt'
    else:
        filename = 'linfitparams_actives' + str(pm) + str(f) + '_run' + str(run) + '.txt'
    filepath = os.path.join(direc_in, filename)
    
    f = open(filepath, "r")
    f.readline()
    f.readline()
    x = []
#    k = 0
#    print(np.size(legitimacies)*len(max_jail_sentences))
    for m, ls in enumerate(legitimacies):
        for n, js in enumerate(max_jail_sentences):
            l_str = f.readline()
            js_str = f.readline()
            a_str = f.readline()
            b_str = f.readline()
            f.readline()
#            k+= 1
#            print(l_str[1:])
#            print(k)
            l = float(l_str[1:])
            js = float(js_str[2:])
            a = ufloat_fromstr(a_str[4:])
            b = ufloat_fromstr(b_str[4:])
            temp = [l, js, a, b]
            x.append(temp)
    f.close()
    return x

def read_p(pm, f, run=1):
    if run == 1:
        filename = 'linfitparams_prisoners' + str(pm) + str(f) + '.txt'
    else:
        filename = 'linfitparams_prisoners' + str(pm) + str(f) + '_run' + str(run) + '.txt'
    filepath = os.path.join(direc_in, filename)
    
    f = open(filepath, "r")
    f.readline()
    f.readline()
    x = []
#    k = 0
#    print(np.size(legitimacies)*len(max_jail_sentences))
    for m, ls in enumerate(legitimacies):
        for n, js in enumerate(max_jail_sentences):
            l_str = f.readline()
            js_str = f.readline()
            a_str = f.readline()
            b_str = f.readline()
            f.readline()
#            k+= 1
#            print(l_str[1:])
#            print(k)
            l = float(l_str[1:])
            js = float(js_str[2:])
            a = ufloat_fromstr(a_str[4:])
            b = ufloat_fromstr(b_str[4:])
            temp = [l, js, a, b]
            x.append(temp)
    f.close()
    return x

def legx_a(pm, f, js, run = 1, pris = False):
    legitimacies = []
    ase = []
    for n in np.arange(1, run+1):
        if pris == True:
            x = read_p(pm, f, n)
        else:
            x = read(pm, f, n)
        # x_plot = []
    
        # for i,x_0 in enumerate(x):
        #     j = x_0[1]
        #     if j == js:
        #         x_plot.append(x[i])
        leg = np.zeros(len(x))
        a = unumpy.uarray(np.zeros(len(x)), np.zeros(len(x)))
        for i in range(len(x)):
            leg[i] = x[i][0]
            a[i] = x[i][2]
        legitimacies.append(leg)
        ase.append(a)
    legitimacy = np.zeros(np.size(legitimacies[0]))
    a_f = unumpy.uarray(np.zeros(np.size(legitimacies[0])), np.zeros(np.size(legitimacies[0])))
    k = float(len(legitimacies))
    for leg, a in zip(legitimacies, ase):
        legitimacy += leg/k
        a_f += a/k         
    return legitimacy, a_f

def legx_b(pm, f, js, run = 1, pris = False):
    legitimacies = []
    bse = []
    for n in np.arange(1, run+1):
        if pris == True:
            x = read_p(pm, f, n)
        else:
            x = read(pm, f, n)
        x_plot = []
    
        for i,x_0 in enumerate(x):
            j = x_0[1]
            if j == js:
                x_plot.append(x[i])
        leg = np.zeros(len(x_plot))
        b = unumpy.uarray(np.zeros(len(x_plot)), np.zeros(len(x_plot)))
        for i in range(len(x_plot)):
            leg[i] = x_plot[i][0]
            b[i] = x[i][3]
        legitimacies.append(leg)
        bse.append(b)
    size = np.size(legitimacies[0])
    # print(legitimacies[0])
    # print(size)
    legitimacy = np.zeros(size)
    b_f = unumpy.uarray(np.zeros(size), np.zeros(size))
    # print(np.shape(b_f))
    k = float(len(legitimacies))
    for leg, b in zip(legitimacies, bse):
        legitimacy += leg/k
        b_f += b/k            
    return legitimacy, b_f

def jsx_a(pm, f, l, run = 1, pris = False):
    jail_sent = []
    ase = []
    for n in np.arange(1, run+1):
        if pris == True:
            x = read_p(pm, f, n)
        else:
            x = read(pm, f, n)
#        x_plot = []
#    
#        for i,x_0 in enumerate(x):
#            ls = x_0[0]
#            if l == ls:
#                x_plot.append(x[i])
        js = np.zeros(len(x))
        a = unumpy.uarray(np.zeros(len(x)), np.zeros(len(x))) 
        for i in range(len(x)):
            js[i] = x[i][1]
            a[i] = x[i][2]
        jail_sent.append(js)
        ase.append(a)
    jail_sentences = np.zeros(np.size(jail_sent[0]))
    a_f = unumpy.uarray(np.zeros(np.size(jail_sent[0])), np.zeros(np.size(jail_sent[0])))
    k = float(run)
    for js, a in zip(jail_sent, ase):
        jail_sentences += js/k
        a_f += a/k            
    return jail_sentences, a_f

def jsx_b(pm, f, l, run=1,  pris = False):
    jail_sent = []
    bse = []
    for n in np.arange(1, run+1):
        if pris == True:
            x = read_p(pm, f, n)
        else:
            x = read(pm, f, n)
#        x_plot = []
#    
#        for i,x_0 in enumerate(x):
#            ls = x_0[0]
#            if l == ls:
#                x_plot.append(x[i])
        js = np.zeros(len(x))
        b = unumpy.uarray(np.zeros(len(x)), np.zeros(len(x)))
        for i in range(len(x)):
            js[i] = x[i][1]
            b[i] = x[i][3]
        jail_sent.append(js)
        bse.append(b)
    jail_sentences = np.zeros(np.size(jail_sent[0]))
    b_f = unumpy.uarray(np.zeros(np.size(jail_sent[0])), np.zeros(np.size(jail_sent[0])))
    k = float(run)
    for js, b in zip(jail_sent, bse):
        jail_sentences += js/k
        b_f += b/k            
    return jail_sentences, b_f
    
def plot_vs_leg_a(pm, f, js, run =1, pris=False):
    leg, a = legx_a(pm, f, js, run, pris)
    color_index = indices_color[int(pm)] + int(f)
    plt.errorbar(leg, unumpy.nominal_values(a), yerr = unumpy.std_devs(a), fmt='o', ms=2, color=colors[color_index])
    
def plot_vs_js_a(pm, f, l, run =1, pris=False):
    js, a = jsx_a(pm, f, l, run, pris)
    color_index = indices_color[int(pm)] + int(f)
    plt.errorbar(js, unumpy.nominal_values(a), yerr = unumpy.std_devs(a), fmt='o', ms=2, color=colors[color_index])
    
p0s = np.array([[-0.33, -0.5, 1.1, 0.9], #00 - 0
                    [-0.33, -0.5, 1.1, 0.9],  #10 - 1
                    [0.24053675,0.8838125, 69.89381554, -17.80458923], #20 - 2
                    [-3.512, -0.008067, 1.2196, 0.45293], #21 - 3
                    [0.24053675,0.8838125, 69.89381554,-17.80458923], #30 - 4
                    [-2.5,-0.00994,1.1219,0.35495], #31 - 5
                    [-1.9, -0.05, 1.2, 0.5], #40 - 6
                    [-0.13727204 ,0.8911804, 64.75194666, -10.85925288], #50 - 7
                    [-2.650,-0.00983,1.1834,0.28170]]) #51 - 8

# p0s = np.array([[-0.33, -0.5, 1.1, 0.9], #00 - 0
#                     [-0.33, -0.5, 1.1, 0.9],  #10 - 1
#                     [69.89381554,0.8838125,0.24053675,-17.80458923], #20 - 2
#                     [-0.78165958,1.19178706,-3.28609192,45.33565976], #21 - 3
#                     [69.89381554,0.8838125,0.24053675,-17.80458923], #30 - 4
#                     [-0.98437074,1.16263277,-2.67848747,35.75941122], #31 - 5
#                     [-6.32174063,0.99890583,-0.69310782,31.75332398], #40 - 6
#                     [ 64.75194666,0.8911804,-0.13727204,-10.85925288], #50 - 7
#                     [-0.92686025,1.19874112,-2.81073937,27.95034422]]) #51 - 8
    
def fit_leg(pm, f, js, run =1):
    leg, a = legx_a(pm, f, js, run)
    fit = lambda x, rho, a, b, c: a*(np.abs(b-x))**rho + c
    index = indices_color[int(pm)] + int(f)
#    if int(pm) == 0 or int(pm) == 1:
#        print('guess 1')
#        guess = np.array([-0.33, -0.5, 1.1, 0.9])
#    elif int(pm) == 2 or int(pm) == 3:
#        print('guess 2')
#        guess = np.array([69.89381554,0.8838125,0.24053675,-17.80458923])
#    else:
#        print('guess 3')
#        guess = np.array([-1.7, -0.07, 1.25, 0.42])
    guess = p0s[index]
    # plt.figure()
    # plt.plot(leg, unumpy.nominal_values(a), 'o', ms=2)
    # plt.plot(leg, fit(leg, *guess))
    # plt.show()
    # plt.clf()
    param, covmat = curve_fit(fit, leg, unumpy.nominal_values(a), p0 = guess)
    return param, covmat

def fit_js(pm, f, l, run =1):
    js, a = jsx_a(pm, f, l, run)
    fit = lambda x, a, b, c, d: a*np.exp(-b*(x-c)) + d
    guess = np.array([0.5, 0.024701, 0, 0.051])
    param, covmat = curve_fit(fit, js, unumpy.nominal_values(a), p0 = guess)
    return param, covmat

def fit_leg_txt(pm, f, js, run =1):
    param, covmat = fit_leg(pm, f, js, run)
    params = unumpy.uarray(param, np.sqrt(np.diag(covmat)))
    filepath_fit = os.path.join(direc_out, "offset_vs_leg_fitparam.txt")
    file = open(filepath_fit, 'a')
    textstr = '\n'.join((
            "pm" + str(pm),
            "f" + str(f),
            "js" + str(js),
            r'a = {:.2u}'.format(params[1]),
            r'b = {:.2u}'.format(params[2]),
            r'rho = {:.2u}'.format(params[0]),
            r'c = {:.2u}'.format(params[3]),            
            '\n'
            ))
    file.write(textstr)
    file.close()


def fit_js_txt(pm, f, l, run =1):
    param, covmat = fit_js(pm, f, l, run)
    params = unumpy.uarray(param, np.sqrt(np.diag(covmat)))
    filepath_fit = os.path.join(direc_out, "offset_vs_js_fitparam.txt")
    file = open(filepath_fit, 'a')
    textstr = '\n'.join((
            "pm" + str(pm),
            "f" + str(f),
            "l" + str(l),
            r'a = {:.2u}'.format(params[0]),
            r'b = {:.2u}'.format(params[1]),
            r'c = {:.2u}'.format(params[2]),
            r'd = {:.2u}'.format(params[3]),            
            '\n'
            ))
    file.write(textstr)
    file.close()
    
def fit_leg_plot(pm, f, js, run =1):
    param, covmat = fit_leg(pm, f, js, run)
    leg, a = legx_a(pm, f, js, run)
    fit = lambda x, rho, a, b, c: a*(np.abs(b-x))**rho + c
    y_fit = fit(leg, *param)
    color_index = indices_color[int(pm)] + int(f)
    plt.errorbar(leg, unumpy.nominal_values(a), yerr = unumpy.std_devs(a), fmt='o', ms=2, color=colors[color_index])
    plt.plot(leg, y_fit, color=colors[color_index])
    
def fit_leg_plot_only(pm, f, js, run=1):
    param, covmat = fit_leg(pm, f, js, run)
    leg, a = legx_a(pm, f, js, run)
    fit = lambda x, rho, a, b, c: a*(np.abs(b-x))**rho + c
    y_fit = fit(leg, *param)
    color_index = indices_color[int(pm)] + int(f)
    plt.plot(leg, y_fit, color=colors[color_index])

    
def fit_js_plot(pm, f, l, run =1):
    param, covmat = fit_js(pm, f, l, run)
    js, a = jsx_a(pm, f, l, run)
    fit = lambda x, a, b, c, d: a*np.exp(-b*(x-c)) + d
    y_fit = fit( js, *param)
    color_index = indices_color[int(pm)] + int(f)
    plt.errorbar(js, unumpy.nominal_values(a), yerr = unumpy.std_devs(a), fmt='o', ms=2, color=colors[color_index])
    plt.plot(js, y_fit, color=colors[color_index])
    
def fit_js_plot_only(pm, f, l, run=1):
    param, covmat = fit_js(pm, f, l, run)
    js, a = jsx_a(pm, f, l, run)
    fit = lambda x, a, b, c, d: a*np.exp(-b*(x-c)) + d
    y_fit = fit( js, *param)
    color_index = indices_color[int(pm)] + int(f)
    plt.plot(js, y_fit, color=colors[color_index])
    

def plot_vs_leg_b(pm, f, js, run =1, pris=False):
    leg, b = legx_b(pm, f, js, run, pris)       
    color_index = indices_color[int(pm)] + int(f)
    plt.errorbar(leg, unumpy.nominal_values(b), yerr = unumpy.std_devs(b), fmt='o', ms=2, color=colors[color_index])

def plot_vs_js_b(pm, f, l, run =1, pris=False):
    js, b = jsx_b(pm, f, l, run, pris)
    color_index = indices_color[int(pm)] + int(f)
    plt.errorbar(js, unumpy.nominal_values(b), yerr = unumpy.std_devs(b), fmt='o', ms=2, color=colors[color_index])
    
def plot_prisoners_js(i_mm, run =1):
    if i_mm == 'True':
         
        for pm0 in pm_macro:
            plot_vs_js_a(pm0, 0, js, run, pris = True)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'prisoners_linplot_offset_macro_f0_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0 in pm_macro:
            plot_vs_js_a(pm0, 1, js,  run,pris = True)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'prisoners_linplot_offset_macro_f1_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0, f0 in zip(pm_micro, f_micro):
            plot_vs_js_a(pm0, f0, js,  run,pris = True)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'prisoners_linplot_offset_micro_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0 in pm_macro:
            plot_vs_js_b(pm0, 0, js, run, pris = True)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Slope')
        plt.tight_layout()
        filename_b = os.path.join(direc_out, 'prisoners_linplot_slope_macro_f0_js' + str(js) + '.eps')
        plt.savefig(filename_b)
        plt.clf()
         
        for pm0 in pm_macro:
            plot_vs_js_b(pm0, 1, js, run, pris = True)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Slope')
        plt.tight_layout()
        filename_b = os.path.join(direc_out, 'prisoners_linplot_slope_macro_f1_js' + str(js) + '.eps')
        plt.savefig(filename_b)
        plt.clf()
         
        for pm0, f0 in zip(pm_micro, f_micro):
            plot_vs_js_b(pm0, f0, js, run, pris = True)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Slope')
        plt.tight_layout()
        filename_b = os.path.join(direc_out, 'prisoners_linplot_slope_micro_js' + str(js) + '.eps')
        plt.savefig(filename_b)
        plt.clf()
    else:
         
        for pm0, f0 in zip(pm_all, f_all):
            plot_vs_js_a(pm0, f0, js, run, pris=True)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'prisoners_linplot_offset_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0, f0 in zip(pm_all, f_all):
            plot_vs_js_b(pm0, f0, js, run, pris=True)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Slope')
        plt.tight_layout()
        filepath_b = os.path.join(direc_out, 'prisoners_linplot_slope_js' + str(js) + '.eps')
        plt.savefig(filepath_b)
        plt.clf()

def plot_prisoners_leg(i_mm, run = 1):
    if i_mm == 'True':
         
        for pm0 in pm_macro:
            plot_vs_leg_a(pm0, 0, js, run, pris = True)
        plt.xlabel('Legitimacy')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'prisoners_linplot_offset_macro_f0_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0 in pm_macro:
            plot_vs_leg_a(pm0, 1, js, run, pris = True)
        plt.xlabel('Legitimacy')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'prisoners_linplot_offset_macro_f1_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0, f0 in zip(pm_micro, f_micro):
            plot_vs_leg_a(pm0, f0, js, run, pris = True)
        plt.xlabel('Legitimacy')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'prisoners_linplot_offset_micro_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0 in pm_macro:
            plot_vs_leg_b(pm0, 0, js, run, pris = True)
        plt.xlabel('Legitimacy')
        plt.ylabel('Slope')
        plt.tight_layout()
        filename_b = os.path.join(direc_out, 'prisoners_linplot_slope_macro_f0_js' + str(js) + '.eps')
        plt.savefig(filename_b)
        plt.clf()
         
        for pm0 in pm_macro:
            plot_vs_leg_b(pm0, 1, js, run, pris = True)
        plt.xlabel('Legitimacy')
        plt.ylabel('Slope')
        plt.tight_layout()
        filename_b = os.path.join(direc_out, 'prisoners_linplot_slope_macro_f1_js' + str(js) + '.eps')
        plt.savefig(filename_b)
        plt.clf()
         
        for pm0, f0 in zip(pm_micro, f_micro):
            plot_vs_leg_b(pm0, f0, js, run, pris = True)
        plt.xlabel('Legitimacy')
        plt.ylabel('Slope')
        plt.tight_layout()
        filename_b = os.path.join(direc_out, 'prisoners_linplot_slope_micro_js' + str(js) + '.eps')
        plt.savefig(filename_b)
        plt.clf()
    else:
         
        for pm0, f0 in zip(pm_all, f_all):
            plot_vs_leg_a(pm0, f0, js, run, pris = True)
        plt.xlabel('Legitimacy')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'prisoners_linplot_offset_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0, f0 in zip(pm_all, f_all):
            plot_vs_leg_b(pm0, f0, js, run, pris = True)
        plt.xlabel('Legitimacy')
        plt.ylabel('Slope')
        plt.tight_layout()
        filepath_b = os.path.join(direc_out, 'prisoners_linplot_slope_js' + str(js) + '.eps')
        plt.savefig(filepath_b)
        plt.clf() 


    


#Police movement and forced line types for all    
pm_all = [0, 1, 2, 2, 3, 3, 4, 5, 5]
f_all = [0, 0, 0, 1, 0, 1, 0, 0, 1]

pm_macro = [2, 3, 5]
f_macro = [0, 1, 0, 1, 0, 1]
pm_micro = [0, 1, 4]
f_micro = [0, 0, 0]

if fitting == 'js':     
    plot_prisoners_js(i_mm, run)
elif fitting == 'leg':
    plot_prisoners_leg(i_mm, run)

if fitting == 'js':
    if i_mm == 'True':
         
        for pm0 in pm_macro:
            plot_vs_js_a(pm0, 0, l,  run)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'linplot_offset_macro_f0_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0 in pm_macro:
            plot_vs_js_a(pm0, 1,  l, run)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'linplot_offset_macro_f1_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0, f0 in zip(pm_micro, f_micro):
            plot_vs_js_a(pm0, f0,  l,  run)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'linplot_offset_micro_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0 in pm_macro:
            plot_vs_js_b(pm0, 0,  l,  run)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Slope')
        plt.tight_layout()
        filename_b = os.path.join(direc_out, 'linplot_slope_macro_f0_js' + str(js) + '.eps')
        plt.savefig(filename_b)
        plt.clf()
         
        for pm0 in pm_macro:
            plot_vs_js_b(pm0, 1,  l,  run)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Slope')
        plt.tight_layout()
        filename_b = os.path.join(direc_out, 'linplot_slope_macro_f1_js' + str(js) + '.eps')
        plt.savefig(filename_b)
        plt.clf()
         
        for pm0, f0 in zip(pm_micro, f_micro):
            plot_vs_js_b(pm0, f0,  l,  run)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Slope')
        plt.tight_layout()
        filename_b = os.path.join(direc_out, 'linplot_slope_micro_js' + str(js) + '.eps')
        plt.savefig(filename_b)
        plt.clf()
        if i_fit == 'True':
             
            for pm0 in pm_macro:
                fit_js_plot(pm0, 0,  l,  run)
                fit_js_txt(pm0, 0,  l,  run)
            plt.xlabel('Maximum Jail Sentence Length')
            plt.ylabel('Equilibrium Position')
            filename_a = os.path.join(direc_out, 'linplot_offset_macro_f0_js' + str(js) + '_fit.eps')
            plt.savefig(filename_a)
            plt.clf()
             
            for pm0 in pm_macro:
                fit_js_plot(pm0, 1,  l,  run)
                fit_js_txt(pm0, 1,  l,  run)
            plt.xlabel('Maximum Jail Sentence Length')
            plt.ylabel('Equilibrium Position')
            filename_a = os.path.join(direc_out, 'linplot_offset_macro_f1_js' + str(js) + '_fit.eps')
            plt.savefig(filename_a)
            plt.clf()
             
            for pm0, f0 in zip(pm_micro, f_micro):
                fit_js_plot(pm0, f0,  l,  run)
                fit_js_txt(pm0, f0,  l,  run)
            plt.xlabel('Maximum Jail Sentence Length')
            plt.ylabel('Equilibrium Position')
            filename_a = os.path.join(direc_out, 'linplot_offset_micro_js' + str(js) + '_fit.eps')
            plt.savefig(filename_a)
            plt.clf()
    

    elif i_all=='True' and i_mm == 'False':
         
        for pm0, f0 in zip(pm_all, f_all):
            plot_vs_js_a(pm0, f0,  l,  run)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'linplot_offset_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0, f0 in zip(pm_all, f_all):
            plot_vs_js_b(pm0, f0,  l,  run)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Slope')
        plt.tight_layout()
        filepath_b = os.path.join(direc_out, 'linplot_slope_js' + str(js) + '.eps')
        plt.savefig(filepath_b)
        plt.clf()
        if i_fit == 'True':
             
            for pm0, f0 in zip(pm_all, f_all):
                fit_js_plot(pm0, f0, l,  run)
                fit_js_txt(pm0, f0,  l,  run)
            plt.xlabel('Maximum Jail Sentence Length')
            plt.ylabel('Equilibrium Position')
            filename_a = os.path.join(direc_out, 'linplot_offset_js' + str(js) + '_fit.eps')
            plt.savefig(filename_a)
            plt.show()
            plt.clf()
            plt.figure(figsize=(12, 8))
            for pm1, pm2 in zip(pm_macro, pm_micro):
                fit_js_plot_only(pm1, 1, l, run)
                fit_js_plot_only(pm2, 0, l, run)
            plt.xlabel('Maximum Jail Sentence Length')
            plt.ylabel('Equilibrium Position')
            filename_jpg = os.path.join(direc_out, 'linplot_offset_allfit_l' + str(l) + '_fit .jpg')
            plt.savefig(filename_jpg)
            plt.clf()
            for pm0, f0 in zip(pm_all, f_all):
                fit_js_plot_only(pm0, f0, l, run)
            plt.xlabel('Maximum Jail Sentence Length')
            plt.ylabel('Equilibrium Position')
            filename_eps = os.path.join(direc_out, 'linplot_offset_allfit_l' + str(l) + '_fit.eps')
            plt.savefig(filename_eps)
            plt.clf()
            
        
    
    elif i_all=='False' and i_mm == 'False':
         
        plot_vs_js_a(pm, f,  l,  run)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Equilibrium Position')
        filename_a = 'linplot_offset_pm' + str(pm) + '_f' + str(f) + '_js' + str(js) + '.eps'
        filepath_a = os.path.join(direc_out, filename_a)
        plt.savefig(filepath_a)
        plt.clf()
         
        plot_vs_js_b(pm, f,  l,  run)
        plt.xlabel('Maximum Jail Sentence Length')
        plt.ylabel('Slope')
        plt.tight_layout()
        filename_b = 'linplot_slope_pm' + str(pm) + '_f' + str(f) + '_js' + str(js) + '.eps'
        filepath_b = os.path.join(direc_out, filename_b)
        plt.savefig(filepath_b)
        plt.clf()
        if i_fit == 'True':
             
            fit_js_plot(pm, f,  l,  run)
            fit_js_txt(pm, f,  l,  run)
            plt.xlabel('Maximum Jail Sentence Length')
            plt.ylabel('Equilibrium Position')
            filename_a = 'linplot_offset_pm' + str(pm) + '_f' + str(f) + '_js' + str(js) + '_fit.eps'
            filepath_a = os.path.join(direc_out, filename_a)
            plt.savefig(filepath_a)
            plt.clf()
            

else:
    if i_mm == 'True':
         
        for pm0 in pm_macro:
            plot_vs_leg_a(pm0, 0, js, run)
        plt.xlabel('Legitimacy')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'linplot_offset_macro_f0_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0 in pm_macro:
            plot_vs_leg_a(pm0, 1, js, run)
        plt.xlabel('Legitimacy')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'linplot_offset_macro_f1_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0, f0 in zip(pm_micro, f_micro):
            plot_vs_leg_a(pm0, f0, js, run)
        plt.xlabel('Legitimacy')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'linplot_offset_micro_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0 in pm_macro:
            plot_vs_leg_b(pm0, 0, js, run)
        plt.xlabel('Legitimacy')
        plt.ylabel('Slope')
        plt.tight_layout()
        filename_b = os.path.join(direc_out, 'linplot_slope_macro_f0_js' + str(js) + '.eps')
        plt.savefig(filename_b)
        plt.clf()
         
        for pm0 in pm_macro:
            plot_vs_leg_b(pm0, 1, js, run)
        plt.xlabel('Legitimacy')
        plt.ylabel('Slope')
        plt.tight_layout()
        filename_b = os.path.join(direc_out, 'linplot_slope_macro_f1_js' + str(js) + '.eps')
        plt.savefig(filename_b)
        plt.clf()
         
        for pm0, f0 in zip(pm_micro, f_micro):
            plot_vs_leg_b(pm0, f0, js, run)
        plt.xlabel('Legitimacy')
        plt.ylabel('Slope')
        plt.tight_layout()
        filename_b = os.path.join(direc_out, 'linplot_slope_micro_js' + str(js) + '.eps')
        plt.savefig(filename_b)
        plt.clf()
        if i_fit == 'True':
            #  
            # for pm0 in pm_macro:
            #     fit_leg_plot(pm0, 0, js, run)
            #     fit_leg_txt(pm0, 0, js, run)
            # plt.xlabel('Legitimacy')
            # plt.ylabel('Equilibrium Position')
            # plt.title('Fraction of Actives')
            # filename_a = os.path.join(direc_out, 'linplot_offset_macro_f0_js' + str(js) + '_fit.eps')
            # plt.savefig(filename_a)
            # plt.clf()
             
            for pm0 in pm_macro:
                fit_leg_plot(pm0, 1, js, run)
                fit_leg_txt(pm0, 1, js, run)
            plt.xlabel('Legitimacy')
            plt.ylabel('Equilibrium Position')
            filename_a = os.path.join(direc_out, 'linplot_offset_macro_f1_js' + str(js) + '_fit.eps')
            plt.savefig(filename_a)
            plt.clf()
             
            for pm0 in pm_micro:
                fit_leg_plot(pm0, 0, js, run)
                fit_leg_txt(pm0, 0, js, run)
            plt.xlabel('Legitimacy')
            plt.ylabel('Equilibrium Position')
            filename_a = os.path.join(direc_out, 'linplot_offset_micro_js' + str(js) + '_fit.eps')
            plt.savefig(filename_a)
            plt.clf()
    

    elif i_all=='True' and i_mm == 'False':
         
        for pm0, f0 in zip(pm_all, f_all):
            plot_vs_leg_a(pm0, f0, js, run)
        plt.xlabel('Legitimacy')
        plt.ylabel('Equilibrium Position')
        filename_a = os.path.join(direc_out, 'linplot_offset_js' + str(js) + '.eps')
        plt.savefig(filename_a)
        plt.clf()
         
        for pm0, f0 in zip(pm_all, f_all):
            plot_vs_leg_b(pm0, f0, js, run)
        plt.xlabel('Legitimacy')
        plt.ylabel('Slope')
        plt.tight_layout()
        filepath_b = os.path.join(direc_out, 'linplot_slope_js' + str(js) + '.eps')
        plt.savefig(filepath_b)
        plt.clf()
        if i_fit == 'True':
             
            for pm0, f0 in zip(pm_all, f_all):
                fit_leg_plot(pm0, f0, js, run)
                fit_leg_txt(pm0, f0, js, run)
            plt.xlabel('Legitimacy')
            plt.ylabel('Equilibrium Position')
            filename_a = os.path.join(direc_out, 'linplot_offset_js' + str(js) + '_fit.eps')
            plt.savefig(filename_a)
            plt.clf()
            plt.clf()
            plt.figure()
            for pm1, pm2 in zip(pm_macro, pm_micro):
                fit_leg_plot_only(pm1, 1, js, run)
                fit_leg_plot_only(pm2, 0, js, run)
                fit_leg_txt(pm1, 1, js, run)
                fit_leg_txt(pm2, 0, js, run)
            for pm0, f0 in zip(pm_all, f_all):
                plot_vs_leg_a(pm0, f0, js, run)
            plt.xlabel('Legitimacy')
            plt.ylabel('Equilibrium Position')
            filename_eps = os.path.join(direc_out, 'linplot_offset_js' + str(js) + '_fit2.eps')
            plt.savefig(filename_eps)
            plt.show()
            plt.clf()
            plt.figure(figsize=(12, 8))
            for pm1, pm2 in zip(pm_macro, pm_micro):
                fit_leg_plot_only(pm1, 1, js, run)
                fit_leg_plot_only(pm2, 0, js, run)
            plt.xlabel('Legitimacy')
            plt.ylabel('Equilibrium Position')
            filename_jpg = os.path.join(direc_out, 'linplot_offset_allfit_js' + str(js) + '_fit .jpg')
            plt.savefig(filename_jpg)
            
        
    
    elif i_all=='False' and i_mm == 'False':
         
        plot_vs_leg_a(pm, f, js, run)
        plt.xlabel('Legitimacy')
        plt.ylabel('Equilibrium Position')
        filename_a = 'linplot_offset_pm' + str(pm) + '_f' + str(f) + '_js' + str(js) + '.eps'
        filepath_a = os.path.join(direc_out, filename_a)
        plt.savefig(filepath_a)
        plt.clf()
         
        plot_vs_leg_b(pm, f, js, run)
        plt.xlabel('Legitimacy')
        plt.ylabel('Slope')
        plt.tight_layout()
        filename_b = 'linplot_slope_pm' + str(pm) + '_f' + str(f) + '_js' + str(js) + '.eps'
        filepath_b = os.path.join(direc_out, filename_b)
        plt.savefig(filepath_b)
        plt.clf()
        if i_fit == 'True':
            
            fit_leg_plot(pm, f, js, run)
            plt.xlabel('Legitimacy')
            plt.ylabel('Equilibrium Position')
            filename_a = 'linplot_offset_pm' + str(pm) + '_f' + str(f) + '_js' + str(js) + '_fit.eps'
            filepath_a = os.path.join(direc_out, filename_a)
            plt.savefig(filepath_a)
            plt.show()
            plt.clf()
            fit_leg_txt(pm, f, js)

    else:
        print("Error: Something really odd went wrong")