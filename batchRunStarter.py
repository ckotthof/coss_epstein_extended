# Created 2020 by Group "Left as an Exercise to the Reader"
# for 851-0101-86L "Complex Social Systems: Modeling Agents, Learning, and Games"
# Semester: Winter 2020/2021
# ETH Zurich
#
# with help of Mesa framework
# based on Epstein's Civil Violence model

import sys
import os
#starts all combinations with the specified parameters down below and the command line given police_movement_type and forced_line
if (len(sys.argv)!=3):
	print("\nCall this program by 'python batchRunStarter.py <police_movement_type> <forced_line>'\npolice_movement_value is an int\nforced_line is True or False\n")
else:
	# you **could** change these values...
	legitimacies = ["0.67", "0.76", "0.85"]
	max_jail_sentences = ["30", "45"]
	citizen_movements = ["0"]
	sampleruns = "1"
	iterations_per_model = "10000"
	#...

	police_movement_type = sys.argv[1]
	forced_line = sys.argv[2]

	if forced_line != "True" and forced_line != "False":
		print("\nvalue for forced_line must be True or False\n")
		exit()

	possibilities = str(len(legitimacies)*len(max_jail_sentences)*len(citizen_movements))
	current_iteration = 1

	for l in legitimacies:
		for js in max_jail_sentences:
			for cm in citizen_movements:
				print("+++++++++++++++++++ running combination "+str(current_iteration)+"/"+possibilities+" +++++++++++++++++++")
				os.system("python runBatch.py mean=True sampleruns="+sampleruns+" max_iters="+iterations_per_model+" legitimacy="+l+" max_jail_sentence="+js+" citizen_movement_type="+cm+" police_movement_type="+police_movement_type+" strong_formation="+forced_line)
				current_iteration += 1
				
# python runBatch.py mean=True sampleruns= max_iters= legitimacy= max_jail_sentence= citizen_movement_type= police_movement_type= strong_formation=
