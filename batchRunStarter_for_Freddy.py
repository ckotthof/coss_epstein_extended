# Created 2020 by Group "Left as an Exercise to the Reader"
# for 851-0101-86L "Complex Social Systems: Modeling Agents, Learning, and Games"
# Semester: Winter 2020/2021
# ETH Zurich
#
# with help of Mesa framework
# based on Epstein's Civil Violence model

import sys
import os
import numpy as np
from subprocess import Popen


#program to run many-steps runs.
if (len(sys.argv)!=3):
    print("\nCall this program by 'python batchRunStarter.py <police_movement_type> <forced_line>'\npolice_movement_value is an int\nforced_line is True or False\n")
else:

    #UNCOMMENT TO RUN LEGITIMACY DATA SET
<<<<<<< HEAD
    legitimacies1 = np.arange(0,40,5)
    legitimacies3 = np.arange(40,55,1)
    legitimacies = np.concatenate((legitimacies1, legitimacies3))
=======
    legitimacies_arr1 = np.arange(0,40,5)
    legitimacies_arr2 = np.arange(40,55,1)
    legitimacies_arr3 = np.arange(55,90,0.25)
    legitimacies_arr = np.concatenate((legitimacies_arr1,legitimacies_arr2,legitimacies_arr3))
>>>>>>> b15944558140c127320e0d419924da014e475827
    max_jail_sentences = ["45"]

    # JAIL SENTENCE DATA SET
    """
    legitimacies = np.array([70])
    max_jail_sentences = np.arange(0,200,2)
    """

    citizen_movements = ["0"]
    sampleruns = "1"
    iterations_per_model = "4000"
    #...

    police_movement_type = sys.argv[1]
    forced_line = sys.argv[2]

    if forced_line != "True" and forced_line != "False":
        print("\nvalue for forced_line must be True or False\n")
        exit()


    possibilities = str(len(legitimacies)*len(max_jail_sentences)*len(citizen_movements))
    current_iteration = 0
    array = [None] * 8


    for l in legitimacies:
        for js in max_jail_sentences:
            for cm in citizen_movements:
                print("+++++++++++++++++++ running combination "+str(current_iteration)+"/"+possibilities+" +++++++++++++++++++")
                #os.startfile("python runBatch.py mean=True sampleruns="+sampleruns+" max_iters="+iterations_per_model+" legitimacy="+("%.2f" % (l/100))+" max_jail_sentence="+js+" citizen_movement_type="+cm+" police_movement_type="+police_movement_type+" strong_formation="+forced_line)
                variable = (current_iteration + 1)%8
                array[variable] = Popen(["python", "runBatch.py", "mean=True", "sampleruns="+sampleruns, "max_iters="+iterations_per_model, "legitimacy="+("%.4f" % ((l)/100.0)), "max_jail_sentence="+str(js), "citizen_movement_type="+cm, "police_movement_type="+police_movement_type, "strong_formation="+forced_line])

                if (variable == 0):
                    print("Here")
                    exit_codes = [p.wait() for p in array]
                    print("There")

                current_iteration += 1

# python runBatch.py mean=True sampleruns= max_iters= legitimacy= max_jail_sentence= citizen_movement_type= police_movement_type= strong_formation=
