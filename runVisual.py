# Created 2020 by Group "Left as an Exercise to the Reader"
# for 851-0101-86L "Complex Social Systems: Modeling Agents, Learning, and Games"
# Semester: Winter 2020/2021
# ETH Zurich
#
# with help of Mesa framework
# based on Epstein's Civil Violence model


#running this file with python results in the start of the interactive visualization with the help of the Mesa Framework
from our_code.server import serverstarter
import sys

if (len(sys.argv)!=3):
	#we dont recommend using this option
	print("You can call this program with optional height and width: 'python run.py <height> <width>'")
	serverstarter()
else:
	serverstarter(int(sys.argv[1]),int(sys.argv[2]))
	
